/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import eu.sapere.RangeSeekBar.OnRangeSeekBarChangeListener;

public class BehaviorActivity extends Activity {

	private Spinner spinnerBehaviors;
	private EditText editTextPerR;
	private RangeSeekBar<Integer> rangeSeekBarWeights;
	private TextView textViewWperR;
	private TextView textViewWsocR;
	private TextView textViewWeff;
	private Map<String, String> personalResponse;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.behavior_activity);

		personalResponse = new HashMap<String, String>();
		String[] behaviors = getResources().getStringArray(R.array.behaviors);
		for (String b : behaviors) {
			personalResponse.put(b, "50");
		}
		editTextPerR = (EditText) findViewById(R.id.editTextPerR);
		textViewWsocR = (TextView) findViewById(R.id.textViewWsocR);
		textViewWeff = (TextView) findViewById(R.id.textViewWeff);
		textViewWperR = (TextView) findViewById(R.id.textViewWperR);
		Vector<String> values = Model.instance().getProperties()
				.get(Model.BEHAVIOR);
		rangeSeekBarWeights = new RangeSeekBar<Integer>(
				Model.RANGE_SEEK_BAR_MIN, Model.RANGE_SEEK_BAR_MAX,
				getApplicationContext());
		rangeSeekBarWeights
				.setSelectedMinValue(Integer.parseInt(values.get(0)));
		rangeSeekBarWeights.setSelectedMaxValue(Integer.parseInt(values.get(0))
				+ Integer.parseInt(values.get(1)));
		textViewWsocR.setText(getString(R.string.w_socR) + values.get(0));
		textViewWeff.setText(getString(R.string.w_eff) + values.get(1));
		textViewWperR.setText(getString(R.string.w_perR) + values.get(2));
		rangeSeekBarWeights
				.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
					@Override
					public void onRangeSeekBarValuesChanged(
							RangeSeekBar<?> bar, Integer minValue,
							Integer maxValue) {
						textViewWsocR.setText(getString(R.string.w_socR)
								+ String.valueOf(minValue));
						textViewWeff.setText(getString(R.string.w_eff)
								+ String.valueOf(maxValue - minValue));
						textViewWperR.setText(getString(R.string.w_perR)
								+ String.valueOf(Model.RANGE_SEEK_BAR_MAX
										- maxValue));
					}
				});

		spinnerBehaviors = (Spinner) findViewById(R.id.spinnerBehaviors);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.behaviors, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerBehaviors.setAdapter(adapter);
		spinnerBehaviors
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						String key = (String) (parent.getItemAtPosition(pos));
						String val = personalResponse.get(key);
						editTextPerR.setText(val);
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
					}
				});

		final Button btSaveChanges = (Button) findViewById(R.id.buttonSave);
		btSaveChanges.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String val = editTextPerR.getText().toString();
				personalResponse.put(spinnerBehaviors.getSelectedItem()
						.toString(), val);
				String message = "Weights and value " + editTextPerR.getText()
						+ " saved for Behavior "
						+ spinnerBehaviors.getSelectedItem();
				MessageLogger.instance().log(message);
				Toast.makeText(BehaviorActivity.this, message,
						Toast.LENGTH_LONG).show();
				Vector<String> values = new Vector<String>();
				values.add(textViewWsocR.getText().toString().split(":")[1]);
				values.add(textViewWeff.getText().toString().split(":")[1]);
				values.add(textViewWperR.getText().toString().split(":")[1]);
				Model.instance().updateProperties(Model.BEHAVIOR, values);
			}
		});
		ViewGroup tableLayout = (ViewGroup) findViewById(R.id.tableLayout1);
		tableLayout.addView(rangeSeekBarWeights);

		System.out.println(Model.instance().getProperties());
	}
}