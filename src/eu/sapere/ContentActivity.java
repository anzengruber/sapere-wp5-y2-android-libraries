/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

public class ContentActivity extends Activity implements ModelChangedListener {

	private Model model;
	private TextView textViewContentChannel;
	private TextView textViewContentIndex;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_activity);
		textViewContentChannel = (TextView) findViewById(R.id.textViewContentChannel);
		textViewContentIndex = (TextView) findViewById(R.id.textViewContentIndex);
		model = Model.instance();
		model.addModelChangedListener(this);
	}

	@Override
	public void modelChanged() {
		final Vector<String> contentChannel = model.getProperties().get(
				Model.CONTENT_CHANNEL);
		if (contentChannel != null && contentChannel.size() > 0) {
			Handler refresh = new Handler(Looper.getMainLooper());
			refresh.post(new Runnable() {
				public void run() {
					textViewContentChannel.setText(contentChannel.get(0));
				}
			});
		}
		final Vector<String> contentIndex = model.getProperties().get(
				Model.CONTENT_INDEX);
		if (contentIndex != null && contentIndex.size() > 0) {
			Handler refresh = new Handler(Looper.getMainLooper());
			refresh.post(new Runnable() {
				public void run() {
					textViewContentIndex.setText(contentIndex.get(0));
				}
			});
		}
	}

	@Override
	public void onProfileSendRequest() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPropertyAdded(Property p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropertyRemoved(Property p) {
		// TODO Auto-generated method stub
		
	}
}