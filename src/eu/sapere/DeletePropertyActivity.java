/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class DeletePropertyActivity extends ListActivity implements
		ModelChangedListener {

	private ArrayAdapter<Property> adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.delete_property);
		adapter = new ArrayAdapter<Property>(this,
				android.R.layout.simple_list_item_1);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
		Model.instance().addModelChangedListener(this);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderIcon(R.drawable.ic_launcher);
		menu.setHeaderTitle(getString(R.string.header));
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.context_menu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.delete_property) {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			Property p = (Property) getListView().getItemAtPosition(
					info.position);
			Model.instance().removeProperty(p);
		}
		return true;
	}

	@Override
	public void onPropertyAdded(Property p) {
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onPropertyRemoved(Property p) {
		adapter.notifyDataSetChanged();
	}

	@Override
	public void modelChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProfileSendRequest() {
		// TODO Auto-generated method stub
		
	}
}