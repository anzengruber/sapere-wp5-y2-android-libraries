/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FoodActivity extends Activity {

	private ListView listViewFoodPrefs;
	private ArrayAdapter<Preference> listAdapter;
	private Button buttonUpdateFoodPrefs;
	private Model model;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.food_activity);

		model = Model.instance();

		buttonUpdateFoodPrefs = (Button) findViewById(R.id.buttonUpdateFoodPrefs);
		buttonUpdateFoodPrefs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Vector<String> preferences = new Vector<String>();
				for (int i = 0; i < listViewFoodPrefs.getCount(); i++) {
					Preference pref = (Preference) listViewFoodPrefs
							.getItemAtPosition(i);
					if (pref.isChecked()) {
						preferences.add(pref.toString());
					}
				}
				model.updateProperties(Model.FOOD_PREFS, preferences);
				Toast.makeText(FoodActivity.this, preferences.toString(),
						Toast.LENGTH_LONG).show();
				MessageLogger.instance().log(
						"Selected user preferences: " + preferences.toString());
			}
		});

		listViewFoodPrefs = (ListView) findViewById(R.id.listViewFoodPrefs);
		listViewFoodPrefs
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View item,
							int position, long id) {
						Preference pref = listAdapter.getItem(position);
						pref.toggleChecked();
						PreferenceViewHolder viewHolder = (PreferenceViewHolder) item
								.getTag();
						viewHolder.getCheckBox().setChecked(pref.isChecked());
					}
				});

		String[] strings = getResources().getStringArray(R.array.food_prefs);
		ArrayList<Preference> prefList = new ArrayList<Preference>();
		for (int i = 0; i < strings.length; i++) {
			prefList.add(new Preference(strings[i]));
		}
		listAdapter = new PreferenceArrayAdapter(this, prefList);
		listViewFoodPrefs.setAdapter(listAdapter);
	}

	private class Preference {
		private String name;
		private boolean checked = false;

		public Preference(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public boolean isChecked() {
			return checked;
		}

		public void setChecked(boolean checked) {
			this.checked = checked;
		}

		public String toString() {
			return name;
		}

		public void toggleChecked() {
			checked = !checked;
		}
	}

	private class PreferenceViewHolder {
		private CheckBox checkBox;
		private TextView textView;

		public PreferenceViewHolder(TextView textView, CheckBox checkBox) {
			this.checkBox = checkBox;
			this.textView = textView;
		}

		public CheckBox getCheckBox() {
			return checkBox;
		}

		public TextView getTextView() {
			return textView;
		}
	}

	private class PreferenceArrayAdapter extends ArrayAdapter<Preference> {

		private LayoutInflater inflater;

		public PreferenceArrayAdapter(Context context, List<Preference> prefList) {
			super(context, R.layout.simple_row, R.id.textViewRow, prefList);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Preference pref = (Preference) this.getItem(position);
			CheckBox checkBox;
			TextView textView;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.simple_row, null);
				textView = (TextView) convertView
						.findViewById(R.id.textViewRow);
				checkBox = (CheckBox) convertView
						.findViewById(R.id.checkboxContent);
				convertView
						.setTag(new PreferenceViewHolder(textView, checkBox));
				checkBox.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						Preference pref = (Preference) cb.getTag();
						pref.setChecked(cb.isChecked());
					}
				});
			} else {
				PreferenceViewHolder viewHolder = (PreferenceViewHolder) convertView
						.getTag();
				checkBox = viewHolder.getCheckBox();
				textView = viewHolder.getTextView();
			}
			checkBox.setTag(pref);
			checkBox.setChecked(pref.isChecked());
			textView.setText(pref.getName());
			return convertView;
		}
	}
}