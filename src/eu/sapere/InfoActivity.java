/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import eu.sapere.R.id;

public class InfoActivity extends Activity implements LogChangedListener,
		ModelChangedListener {

	private static final long DELAY = 30000;

	private Handler logHandler;
	private EditText editTextLog;
	private TextView textViewNodeName;
	private TextView textViewTarget;
	private TextView textViewSCI;
	private Button buttonSendProfile;
	private Timer timer;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info_activity);
		buttonSendProfile = (Button) findViewById(id.buttonSendProfile);
		buttonSendProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Model.instance().requestProfileTransmission();
				timer = new Timer();
				timer.schedule(new SendProfileTask(), DELAY);
			}
		});
		editTextLog = (EditText) findViewById(R.id.editTextLogMessages);
		textViewNodeName = (TextView) findViewById(R.id.textViewNodeName);
		textViewNodeName.setText(Model.instance().getNodeName());
		textViewTarget = (TextView) findViewById(R.id.textViewTarget);
		textViewTarget.setText("-");
		textViewSCI = (TextView) findViewById(R.id.textViewSCI);
		logHandler = new Handler();
		Model.instance().addModelChangedListener(this);
		textViewSCI.setText(Model.instance().getProperties().get(Model.SCI)
				.firstElement());
		MessageLogger.instance().addLogChangedListener(this);
	}

	private class SendProfileTask extends TimerTask {
		public void run() {
			Model.instance().requestProfileTransmission();
			timer.schedule(new SendProfileTask(), DELAY);
		}
	}

	@Override
	public void logChanged(final String log) {
		logHandler.post(new Runnable() {
			@Override
			public void run() {
				editTextLog.setText(log);
				editTextLog.setSelection(editTextLog.getText().length());
			}
		});
	}

	@Override
	public void modelChanged() {
		Map<String, Vector<String>> properties = Model.instance()
				.getProperties();
		final Vector<String> sci = properties.get(Model.SCI);
		if (sci != null && textViewSCI != null) {
			Handler refresh = new Handler(Looper.getMainLooper());
			refresh.post(new Runnable() {
				public void run() {
					textViewSCI.setText(sci.firstElement());
				}
			});
		}
		final Vector<String> steering = properties.get(Model.STEERING);
		if (steering != null && textViewTarget != null) {
			Handler refresh = new Handler(Looper.getMainLooper());
			refresh.post(new Runnable() {
				public void run() {
					textViewTarget
							.setText(steering.firstElement().split(":")[1]);
				}
			});
		}
	}

	@Override
	public void onProfileSendRequest() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPropertyAdded(Property p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropertyRemoved(Property p) {
		// TODO Auto-generated method stub
		
	}
}