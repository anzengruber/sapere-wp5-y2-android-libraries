/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Thomas Schmittner
 * 
 */
public class LsaView extends View {

	private static final int WIDTH = 20;
	private static final int HEIGHT = 20;
	private static final int MARGIN = 220;
	private static final int SPACING = 5;
	private static final int VSPACE = 15;
	private static final int DEFAULT_TEXT_SIZE = 19;
	private Map<String, Map<Integer, Vector<Integer>>> parsedLsas;
	private int currentHeight = 0;
	private Paint paint;

	public LsaView(Context context, AttributeSet attrs) {
		super(context, attrs);
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.FILL);
		paint.setStrokeWidth(3);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(Color.BLACK);
		int nrOfLSA = 0;

		if (parsedLsas != null) {
			for (String key : parsedLsas.keySet()) {
				Map<Integer, Vector<Integer>> tmpPropertyCount = parsedLsas
						.get(key);
				String name = key + "#" + tmpPropertyCount.get(4).elementAt(0);
				Vector<Integer> standardProp = tmpPropertyCount.get(0);
				Vector<Integer> subProperties = tmpPropertyCount.get(1);
				Vector<Integer> bonds = tmpPropertyCount.get(2);
				Vector<Integer> syntheticProp = tmpPropertyCount.get(3);

				int offset = 0;
				for (int i : standardProp) {
					drawRectangle(canvas, 0, name, i, nrOfLSA, offset);
					offset += i;
				}
				for (int i : subProperties) {
					drawRectangle(canvas, 1, name, i + 1, nrOfLSA, offset);
					offset += i + 1;
				}
				for (int i : bonds) {
					drawRectangle(canvas, 2, name, i, nrOfLSA, offset);
					offset += i;
				}
				for (int i : syntheticProp) {
					drawRectangle(canvas, 3, name, i, nrOfLSA, offset);
					offset += i;
				}
				nrOfLSA++;
			}
		}
	}

	private void drawRectangle(Canvas canvas, int type, String name,
			int lsaCount, int row, int offset) {
		int left, top, right, bottom, color = 0;
		switch (type) {
		case 0:
			color = Color.RED;
			break;
		case 1:
			color = Color.WHITE;
			break;
		case 2:
			color = Color.GREEN;
			break;
		case 3:
			color = Color.YELLOW;
			break;
		}
		for (int i = 0; i < lsaCount; i++) {
			int width = WIDTH;
			int height = HEIGHT;
			if (type == 1 && i > 0) {
				width = WIDTH / 2;
				height = HEIGHT / 2;
				left = MARGIN + (offset + i) * WIDTH + (offset + i) * SPACING
						+ WIDTH / 4;
				top = (row + 1) * SPACING + row * HEIGHT + HEIGHT / 4;
				right = left + width;
				bottom = top + height;
			} else {
				left = MARGIN + (offset + i) * width + (offset + i) * SPACING;
				right = left + width;
				top = (row + 1) * SPACING + row * height;
				bottom = top + height;
				paint.setTextSize(DEFAULT_TEXT_SIZE);
				paint.setColor(Color.YELLOW);
				canvas.drawText(name, SPACING, bottom, paint);
			}
			paint.setColor(color);
			canvas.drawRect(left, top, right, bottom, paint);
			currentHeight = bottom + VSPACE;
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(2000, currentHeight);
	}

	public void updateLSAs(Map<String, Map<Integer, Vector<Integer>>> parsedLSAs) {
		this.parsedLsas = parsedLSAs;
		invalidate();
		requestLayout();
	}
}