/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import android.app.TabActivity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TabHost;
import eu.sapere.middleware.node.networking.physical.bluetooth.android.RegistrationThread;

/**
 * @author Thomas Schmittner
 * 
 */
public class MainActivity extends TabActivity {

	private static final boolean LOG_TO_FILE = false;
	private static final String logFile = "/log.txt";
	private static final Object LOCK = new Object();

	private static File downloadDir;

	private TabHost tabHost;
	private Intent middlewareIntent = null;
	private RegistrationThread registration;
	private String btMac;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (LOG_TO_FILE) {
			try {
				downloadDir = Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
				PrintStream stream = new PrintStream(new FileOutputStream(
						downloadDir + logFile, true));
				System.setOut(stream);
				System.setErr(stream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		btMac = BluetoothAdapter.getDefaultAdapter().getAddress();

		// Restore user settings
		SharedPreferences settings = getPreferences(MODE_PRIVATE);
		Model.instance().loadDefaultValues(settings);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab);
		Resources res = getResources();
		tabHost = getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		// user information and log messages
		intent = new Intent().setClass(this, InfoActivity.class);
		spec = tabHost
				.newTabSpec("InfoActivity")
				.setIndicator("User Info", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// user preferences
		intent = new Intent().setClass(this, PrefsActivity.class);
		spec = tabHost
				.newTabSpec("PrefsActivity")
				.setIndicator("User Prefs",
						res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// TODO uncomment to add user preferences activity tab to GUI
		// food preferences
		// intent = new Intent().setClass(this, FoodActivity.class);
		// spec = tabHost
		// .newTabSpec("FoodActivity")
		// .setIndicator("Food Prefs",
		// res.getDrawable(R.drawable.tab_icon))
		// .setContent(intent);
		// tabHost.addTab(spec);

		// behavior
		intent = new Intent().setClass(this, BehaviorActivity.class);
		spec = tabHost.newTabSpec("BehaviorActivity")
				.setIndicator("Behavior", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// steering
		intent = new Intent().setClass(this, SteeringActivity.class);
		spec = tabHost.newTabSpec("SteeringActivity")
				.setIndicator("Steering", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// LSA space visualization
		intent = new Intent().setClass(this, SpaceActivity.class);
		spec = tabHost
				.newTabSpec("SpaceActivity")
				.setIndicator("LSA Space", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// content follows user
		intent = new Intent().setClass(this, ContentActivity.class);
		spec = tabHost.newTabSpec("ContentActivity")
				.setIndicator("Content", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// settings
		intent = new Intent().setClass(this, SettingsActivity.class);
		spec = tabHost.newTabSpec("SettingsActivity")
				.setIndicator("Settings", res.getDrawable(R.drawable.tab_icon))
				.setContent(intent);
		tabHost.addTab(spec);

		// start middleware intent
		middlewareIntent = new Intent(this, MiddlewareIntent.class);
		startService(middlewareIntent);

		String btMac = BluetoothAdapter.getDefaultAdapter().getAddress();
		registration = new RegistrationThread(btMac, Action.REGISTER, null);
		registration.start();
	}

	@Override
	protected void onDestroy() {
		System.out.println(this.getClass() + " onDestroy called");
		super.onDestroy();
		Model.instance().storeUserPreferences();
		RegistrationThread deregister = null;
		if (registration.isRegistered()) {
			System.out.println("Starting deregistration...");
			synchronized (LOCK) {
				deregister = new RegistrationThread(btMac, Action.DEREGISTER,
						LOCK);
				deregister.start();
				try {
					System.out
							.println("Waiting for deregistration to complete...");
					LOCK.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Notified to proceed...");
		}
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}