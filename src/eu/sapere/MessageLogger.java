/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Thomas Schmittner
 * 
 */
public class MessageLogger {

	private static MessageLogger instance;
	private StringBuilder log;
	private List<LogChangedListener> listeners;

	private MessageLogger() {
		log = new StringBuilder();
		listeners = new ArrayList<LogChangedListener>();
	}

	public static MessageLogger instance() {
		if (instance == null) {
			instance = new MessageLogger();
		}
		return instance;
	}

	public void log(String message) {
		log.append("[" + System.currentTimeMillis() + "] ");
		log.append(message);
		log.append("\n");
		fireLogChanged();
	}

	public void addLogChangedListener(LogChangedListener l) {
		listeners.add(l);
	}

	public void removeLogChangedListener(LogChangedListener l) {
		listeners.remove(l);
	}

	private void fireLogChanged() {
		for (LogChangedListener l : listeners) {
			l.logChanged(log.toString());
		}
	}
}