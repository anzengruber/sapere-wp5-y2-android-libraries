/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import eu.sapere.SpaceActivity.ConsoleReceiver;
import eu.sapere.android.sensors.ServiceProvider;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.node.NodeManager;

public class MiddlewareIntent extends IntentService {

	public static final String PARAM_IN_MSG = "imsg";
	public static final String PARAM_OUT_MSG = "omsg";
	public static final String PIC_URL = "sapere.intent.action.URL_RECEIVED";

	private static Context context;
	private static MiddlewareIntent myinstance;

	private final Object LOCK = new Object();
	private NodeManager sm;

	public MiddlewareIntent() {
		super("SimpleIntentService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		myinstance = this;
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	protected void onHandleIntent(Intent arg0) {
		myinstance = this;
		new ServiceProvider(getApplicationContext(), this);
		sm = NodeManager.instance();
		sm.startNode(Model.instance().getNodeName());
		Model.instance().execute("");
		synchronized (LOCK) {
			try {
				LOCK.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		sm.stopServices();
		Log.d("MW intent", "MW intent shut down!");
	}

	public static Context getContext() {
		return context;
	}

	public static MiddlewareIntent getInstance() {
		return myinstance;
	}

	public void sendMsg(String s[]) {
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(ConsoleReceiver.ACTION_RESP);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra(PARAM_OUT_MSG, s);
		sendBroadcast(broadcastIntent);
	}

	public void update(final Lsa[] list) {
		String result[] = new String[list.length];
		for (int i = 0; i < list.length; i++) {
			result[i] = list[i].toString();
		}
		sendMsg(result);
	}

	public void updateURL(String s) {
		Log.d("Intent", "Url sent");
	}
}