/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

import android.content.SharedPreferences;
import android.os.AsyncTask;

/**
 * @author Thomas Schmittner
 * 
 */
public class Model extends AsyncTask<String, Void, Vector<String>> {

	public static final String SCI = "sci";
	public static final String STEERING = "steering";
	public static final String FOOD_PREFS = "food preferences";
	public static final String USER_PREFS = "personal content";
	public static final String BEHAVIOR = "behavior";
	public static final String CONTENT_CHANNEL = "content channel";
	public static final String CONTENT_INDEX = "content index";
	public static final String NODE_NAME = "node name";
	public static final int RANGE_SEEK_BAR_MIN = 0;
	public static final int RANGE_SEEK_BAR_MAX = 100;

	private static final String DEFAULT = "default";
	private static final String VERSION = "version";
	private static final String RED_BULL_TV = "Red Bull TV-Fenster";
	private static final String DEFAULT_WEIGHTS = "33;34;33";
	private static long version = 0;
	private static Model instance;

	private SharedPreferences settings;
	private Map<String, Vector<String>> properties = new HashMap<String, Vector<String>>();
	private List<ModelChangedListener> listeners;
	private String nodeName;
	private Vector<String> contentChannels;

	// private String ipAddress;

	private Model() {
		listeners = new ArrayList<ModelChangedListener>();
		properties = new HashMap<String, Vector<String>>();
	}

	public static Model instance() {
		if (instance == null) {
			instance = new Model();
		}
		return instance;
	}

	public void updateProperties(String key, Vector<String> values) {
		properties.put(key, values);
		fireModelChanged();
	}

	public void requestProfileTransmission() {
		for (ModelChangedListener l : listeners) {
			l.onProfileSendRequest();
		}
	}

	private void fireModelChanged() {
		for (ModelChangedListener l : listeners) {
			l.modelChanged();
		}
	}

	public Map<String, Vector<String>> getProperties() {
		return properties;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String name) {
		nodeName = name;
	}

	public Vector<String> getContentChannels() {
		return contentChannels;
	}

	public void addModelChangedListener(ModelChangedListener l) {
		listeners.add(l);
	}

	public void removeModelChangedListener(ModelChangedListener l) {
		listeners.remove(l);
	}

	public void updateVersion() {
		Vector<String> value = new Vector<String>();
		value.add(String.valueOf(version++));
		properties.put(VERSION, value);
	}

	public void loadDefaultValues(SharedPreferences settings) {
		this.settings = settings;

		// node name
		nodeName = settings.getString(NODE_NAME, DEFAULT);

		// last viewed content channel
		Vector<String> value = new Vector<String>();
		value.add(settings.getString(CONTENT_CHANNEL, ""));
		properties.put(CONTENT_CHANNEL, value);

		// index of the last viewed content page
		value = new Vector<String>();
		value.add(settings.getString(CONTENT_INDEX, ""));
		properties.put(CONTENT_INDEX, value);

		// individual social capital
		value = new Vector<String>();
		value.add(settings.getString(SCI, "0"));
		properties.put(SCI, value);

		// user preferences
		value = parseCsvString(settings.getString(USER_PREFS, RED_BULL_TV)
				.toString());
		properties.put(USER_PREFS, value);

		// behavior weights
		value = parseCsvString(settings.getString(BEHAVIOR, DEFAULT_WEIGHTS));
		properties.put(BEHAVIOR, value);

		// load available content
		// contentChannels = getAvailableChannels();

		System.out.println(properties.toString());
	}

	private Vector<String> parseCsvString(String value) {
		Vector<String> values = new Vector<String>();
		for (String s : value.split(";")) {
			values.add(s);
		}
		return values;
	}

	private String createCsvString(Vector<String> values) {
		StringBuilder builder = new StringBuilder();
		for (String s : values) {
			builder.append(s);
			builder.append(";");
		}
		return builder.toString();
	}

	public void storeUserPreferences() {
		SharedPreferences.Editor editor = settings.edit();
		// editor.clear();
		editor.putString(CONTENT_CHANNEL, properties.get(CONTENT_CHANNEL)
				.firstElement());
		editor.putString(CONTENT_INDEX, properties.get(CONTENT_INDEX)
				.firstElement());
		editor.putString(USER_PREFS,
				createCsvString(properties.get(USER_PREFS)));
		editor.putString(SCI, properties.get(SCI).firstElement());
		editor.putString(BEHAVIOR, createCsvString(properties.get(BEHAVIOR)));
		editor.putString(NODE_NAME, nodeName);
		editor.commit();
	}

	// public String getIpAddress() {
	// return ipAddress;
	// }
	//
	// private String getLocalIpAddress() {
	// String ipAddressv4 = null;
	// while (ipAddressv4 == null) {
	// try {
	// for (Enumeration<NetworkInterface> en = NetworkInterface
	// .getNetworkInterfaces(); en.hasMoreElements();) {
	// NetworkInterface intf = en.nextElement();
	// for (Enumeration<InetAddress> enumIpAddr = intf
	// .getInetAddresses(); enumIpAddr.hasMoreElements();) {
	// InetAddress inetAddress = enumIpAddr.nextElement();
	// if (!inetAddress.isLoopbackAddress()
	// && InetAddressUtils
	// .isIPv4Address(ipAddressv4 = inetAddress
	// .getHostAddress())) {
	// }
	// }
	// }
	// } catch (SocketException e) {
	// e.printStackTrace();
	// }
	// }
	// return ipAddressv4;
	// }

	@Override
	protected void onPostExecute(Vector<String> channels) {
		contentChannels = channels;
	}

	@Override
	protected Vector<String> doInBackground(String... params) {
		boolean documentParsed = false;
		Document document = null;
		URL url;
		Tidy tidy = new Tidy();
		tidy.setXmlTags(true);
		tidy.setAsciiChars(false);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.setWraplen(Integer.MAX_VALUE);
		tidy.setXmlOut(true);
		Vector<String> channels = new Vector<String>();

		while (!documentParsed) {
			try {
				url = new URL("http://ios.servustv.com/testflight-vod2vju-xml");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						url.openConnection().getInputStream(), "UTF-8"));
				document = tidy.parseDOM(in, null);
				document.setXmlVersion("1.0");
				NodeList nl = document.getElementsByTagName("channel");
				for (int i = 0; i < nl.getLength(); i++) {
					NamedNodeMap attr = nl.item(i).getAttributes();
					for (int j = 0; j < attr.getLength(); j++) {
						channels.add(attr.item(j).getNodeValue());
					}
				}
				documentParsed = true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return channels;
	}

	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		
	}

	public void removeProperty(Property p) {
		// TODO Auto-generated method stub
		
	}
}