/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.TextView;

public class MwConsoleActivity extends Activity {

	public static TextView myView;
	private ConsoleReceiver receiver;
	private Intent msgIntent = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Prevent screen from going to sleep
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.main);

		myView = (TextView) findViewById(R.id.lsa);
		myView.setText("Waiting for LSAs...\n\n");

		IntentFilter filter = new IntentFilter(ConsoleReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ConsoleReceiver();
		registerReceiver(receiver, filter);

		msgIntent = new Intent(this, MiddlewareIntent.class);
		startService(msgIntent);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public class ConsoleReceiver extends BroadcastReceiver {
		public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_PROCESSED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String text[] = intent
					.getStringArrayExtra(MiddlewareIntent.PARAM_OUT_MSG);
			String print = "";

			if (text != null)
				for (String s : text)
					print += s + "\n";

			TextView lsa = (TextView) findViewById(R.id.lsa);
			lsa.setText(print + "\n");
		}
	}

	// The following methods are mandatory to close the APP conveniently

	private void stopServices() {
		Log.d("Quit", "Intento stopped!");
	}

	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("Quit", "onKeyDown called");
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			stopServices();
			stopService(msgIntent);
			onStop();
		}
		return false;

	}

	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
		System.exit(0);
	}

	protected void onStop() {
		super.onStop();
		finish();
	}
}