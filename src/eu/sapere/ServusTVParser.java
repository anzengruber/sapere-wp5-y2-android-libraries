/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

public class ServusTVParser {

	public static Vector<String> getAvailableChannels() {
		boolean documentParsed = false;
		Map<String, Vector<Node>> nextChannelContent = new HashMap<String, Vector<Node>>();
		Document document = null;
		URL url;
		Tidy tidy = new Tidy();
		tidy.setXmlTags(true);
		tidy.setAsciiChars(false);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.setWraplen(Integer.MAX_VALUE);
		tidy.setXmlOut(true);
		while (!documentParsed) {
			try {
				url = new URL("http://ios.servustv.com/testflight-vod2vju-xml");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						url.openConnection().getInputStream(), "UTF-8"));
				document = tidy.parseDOM(in, null);
				document.setXmlVersion("1.0");
				NodeList nl = document.getElementsByTagName("channel");
				for (int i = 0; i < nl.getLength(); i++) {
					NamedNodeMap attr = nl.item(i).getAttributes();
					for (int j = 0; j < attr.getLength(); j++) {
						Vector<Node> entries = getEntries(nl.item(i));
						nextChannelContent.put(attr.item(j).getNodeValue(),
								entries);
					}
				}
				documentParsed = true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(String s : nextChannelContent.keySet()) {
			System.out.println(s + " ---- " + nextChannelContent.get(s));
		}
		return new Vector<String>(nextChannelContent.keySet());
	}

	private static Vector<Node> getEntries(Node channelNode) {
		Vector<Node> entries = new Vector<Node>();
		NodeList formats = channelNode.getChildNodes();
		for (int i = 0; i < formats.getLength(); i++) {
			NodeList tmp_entries = formats.item(i).getChildNodes();
			for (int j = 0; j < tmp_entries.getLength(); j++) {
				entries.add(tmp_entries.item(j));
			}
		}
		return entries;
	}
}