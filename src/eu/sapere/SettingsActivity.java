/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import eu.sapere.R.id;

public class SettingsActivity extends Activity {

	private EditText editTextNodeName;
	private Button buttonSaveChanges;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_activity);

		editTextNodeName = (EditText) findViewById(id.editTextNodeName);
		editTextNodeName.setText(Model.instance().getNodeName());
		buttonSaveChanges = (Button) findViewById(id.buttonSaveNodeName);
		buttonSaveChanges.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String name = editTextNodeName.getText().toString();
				Model.instance().setNodeName(name);
				Toast.makeText(
						SettingsActivity.this,
						"Your SAPERE node name was successfully changed and will be used after a restart of the application",
						Toast.LENGTH_LONG).show();
			}
		});
	}
}