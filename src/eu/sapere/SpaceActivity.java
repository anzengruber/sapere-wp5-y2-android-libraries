/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.TextView;

public class SpaceActivity extends Activity {

	public static TextView textViewLsa;
	public static LsaView lsaView;
	private ConsoleReceiver receiver;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Prevent screen from going to sleep
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.space_activity);

		textViewLsa = (TextView) findViewById(R.id.lsa);
		textViewLsa.setText("Waiting for LSAs...\n\n");

		lsaView = (LsaView) findViewById(R.id.view_lsa_visuals);

		IntentFilter filter = new IntentFilter(ConsoleReceiver.ACTION_RESP);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		receiver = new ConsoleReceiver();
		registerReceiver(receiver, filter);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public class ConsoleReceiver extends BroadcastReceiver {
		public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_PROCESSED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String text[] = intent
					.getStringArrayExtra(MiddlewareIntent.PARAM_OUT_MSG);
			String output = "";

			if (text != null) {
				for (String s : text) {
					output += s + "\n";
				}
			}
			textViewLsa.setText(output + "\n");
			Map<String, Map<Integer, Vector<Integer>>> parsedLsas = parseLsaString(output);
			lsaView.updateLSAs(parsedLsas);
		}
	}

	@SuppressLint("UseSparseArrays")
	private Map<String, Map<Integer, Vector<Integer>>> parseLsaString(
			String currentLSAs) {
		Map<String, Map<Integer, Vector<Integer>>> result = new HashMap<String, Map<Integer, Vector<Integer>>>();
		String[] lsas = currentLSAs.split("\n");

		for (String lsa : lsas) {
			String key = lsa.substring(1, lsa.indexOf(","));
			lsa = lsa.substring(lsa.indexOf(",") + 1);
			String[] properties = lsa.split(",");

			int standardProp = 0;
			int subProperties = 0;
			Vector<Integer> subDescriptions = new Vector<Integer>();
			int bonds = 0;
			int syntheticProp = 0;

			boolean parseStandardProperty = true;
			boolean parseSubProperty = false;

			for (String prop : properties) {
				prop = prop.trim();

				if (parseStandardProperty) {
					if (prop.indexOf("<") != -1) {
						parseSubProperty = true;
						parseStandardProperty = false;
					} else if (prop.indexOf("#") != -1) {
						syntheticProp++;
					} else if (prop.indexOf("[") != -1) {
						standardProp++;
					}
				} else if (parseSubProperty) {
					if (prop.indexOf("#") != -1) {
						bonds++;
					}
					if (prop.indexOf(">") != -1) {
						parseSubProperty = false;
						parseStandardProperty = true;
						subDescriptions.add(subProperties);
						subProperties = 0;
					} else if (prop.indexOf("[") != -1) {
						subProperties++;
					}
				}
			}

			Vector<Integer> stPropV = new Vector<Integer>();
			stPropV.add(standardProp);

			Vector<Integer> bondV = new Vector<Integer>();
			bondV.add(bonds);

			Vector<Integer> syntheticPropV = new Vector<Integer>();
			syntheticPropV.add(syntheticProp);

			Map<Integer, Vector<Integer>> v = new HashMap<Integer, Vector<Integer>>();
			v.put(0, stPropV);
			v.put(1, subDescriptions);
			v.put(2, bondV);
			v.put(3, syntheticPropV);

			Vector<Integer> hashV = new Vector<Integer>();
			hashV.add(key.hashCode());
			v.put(4, hashV);
			result.put(key, v);
		}
		return result;
	}

	// private Map<String, Vector<Integer>> parseLsaString(String currentLSAs) {
	// Map<String, Vector<Integer>> result = new HashMap<String,
	// Vector<Integer>>();
	//
	// while (!currentLSAs.isEmpty()) {
	// int standardProp = 0;
	// int subDescriptions = 0;
	// int bonds = 0;
	// int syntheticProp = 0;
	//
	// int index = currentLSAs.indexOf("\n");
	// String tmpLsa = currentLSAs.substring(0, index);
	// System.out.println("TMP LSA: " + tmpLsa);
	// currentLSAs = currentLSAs.substring(index + 1);
	//
	// index = tmpLsa.indexOf(",");
	// String key = tmpLsa.substring(1, index);
	// tmpLsa = tmpLsa.substring(index + 1);
	//
	// while (!tmpLsa.isEmpty()) {
	// index = tmpLsa.indexOf(",");
	// if (index == -1) {
	// syntheticProp++;
	// break;
	// }
	//
	// switch (getPropertyType(tmpLsa.substring(0, index))) {
	// case 0:
	// standardProp++;
	// break;
	// case 1:
	// subDescriptions++;
	// break;
	// case 2:
	// bonds++;
	// break;
	// case 3:
	// syntheticProp++;
	// break;
	// default:
	// break;
	// }
	// tmpLsa = tmpLsa.substring(index + 1);
	// }
	// Vector<Integer> v = new Vector<Integer>();
	// v.add(standardProp);
	// v.add(subDescriptions);
	// v.add(bonds);
	// v.add(syntheticProp);
	// result.put(key, v);
	// }
	// return result;
	// }
	//
	// private int getPropertyType(String property) {
	// if (property.startsWith(" ")) {
	// property = property.substring(1);
	// }
	// if (property.startsWith("#")) {
	// if (property.startsWith("#bonds")) {
	// return 2;
	// } else {
	// return 3;
	// }
	// } else if (property.indexOf("<") != -1) {
	// return 1;
	// } else if (property.length() > 2) {
	// return 0;
	// } else {
	// return -1;
	// }
	// }

	// The following methods are mandatory to close the APP conveniently

	private void stopServices() {
		Log.d("Quit", "Intento stopped!");
	}

	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("Quit", "onKeyDown called");
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			stopServices();
			onStop();
		}
		return false;
	}

	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
		System.exit(0);
	}

	protected void onStop() {
		super.onStop();
		finish();
	}
}