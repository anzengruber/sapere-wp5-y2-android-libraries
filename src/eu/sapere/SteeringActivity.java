/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere;

import java.util.Vector;

import eu.sapere.middleware.node.NodeManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SteeringActivity extends Activity {

	private ListView listViewTargets;
	private Button buttonStartSteering;
	private Model model;
	private String selectedTarget = null;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.steering_activity);
		model = Model.instance();
		listViewTargets = (ListView) findViewById(R.id.listViewSteeringTargets);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.steering_targets,
				android.R.layout.simple_list_item_single_choice);
		listViewTargets.setAdapter(adapter);
		listViewTargets.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> myAdapter, View myView,
					int myItemInt, long mylng) {
				selectedTarget = (String) (listViewTargets
						.getItemAtPosition(myItemInt));
			}
		});
		buttonStartSteering = (Button) findViewById(R.id.buttonStartSteering);
		buttonStartSteering.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (selectedTarget != null) {
					String spaceName = NodeManager.instance().getSpaceName();
					Vector<String> value = new Vector<String>();
					value.add(spaceName + ":" + selectedTarget);
					model.updateProperties(Model.STEERING, value);
					String message = "Steering to destination "
							+ selectedTarget + " started!";
					MessageLogger.instance().log(message);
					Toast.makeText(SteeringActivity.this, message,
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}