/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.android.sensors;

import java.util.ArrayList;
import java.util.List;

import eu.sapere.android.sensors.listeners.AccelerometerDataListener;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;

/**
 * Android {@linkplain Service} that provides registered listeners with sensor
 * data from the in-built accelerometer of the phone.
 * 
 * @author Thomas Schmittner
 * 
 */
public class AccService extends Service implements SensorEventListener {

	private final Binder localBinder = new LocalBinder();
	private SensorManager sensorManager;
	private Sensor accSensor;
	private List<AccelerometerDataListener> listeners;

	public class LocalBinder extends Binder {
		public AccService getService() {
			return AccService.this;
		}
	}

	@Override
	public void onCreate() {
		listeners = new ArrayList<AccelerometerDataListener>();
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorManager.registerListener(this, accSensor,
				SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return localBinder;
	}

	@Override
	public void onDestroy() {
		sensorManager.unregisterListener(this);
		super.onDestroy();
	}

	public void registerAccelerometerDataListener(AccelerometerDataListener l) {
		listeners.add(l);
	}

	public void unregisterAccelerometerDataListener(AccelerometerDataListener l) {
		listeners.remove(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		for (AccelerometerDataListener l : listeners) {
			l.onAccDataChanged(event);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
	}
}