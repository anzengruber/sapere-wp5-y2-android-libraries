/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.android.sensors;

import java.util.ArrayList;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import eu.sapere.android.sensors.listeners.NwLocationChangedListener;

/**
 * Android {@linkplain Service} that provides registered listeners with location
 * updates based on the network location of the phone.
 * 
 * @author Thomas Schmittner
 * 
 */
public class LocService extends Service implements LocationListener {

	private final Binder localBinder = new LocalBinder();
	private LocationManager locationManager;
	private List<NwLocationChangedListener> listeners;

	public class LocalBinder extends Binder {
		LocService getService() {
			return LocService.this;
		}
	}

	@Override
	public void onCreate() {
		listeners = new ArrayList<NwLocationChangedListener>();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return localBinder;
	}

	@Override
	public void onDestroy() {
		locationManager.removeUpdates(this);
	}

	public void registerLocationChangedListener(NwLocationChangedListener l) {
		listeners.add(l);
	}

	public void unregisterLocationChangedListener(NwLocationChangedListener l) {
		listeners.remove(l);
	}

	@Override
	public void onLocationChanged(Location loc) {
		for (NwLocationChangedListener l : listeners) {
			l.onNwLocationChanged(loc);
		}
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
	}
}