/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.android.sensors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import eu.sapere.MiddlewareIntent;
import eu.sapere.android.sensors.listeners.AccelerometerDataListener;
import eu.sapere.android.sensors.listeners.GpsLocationChangedListener;
import eu.sapere.android.sensors.listeners.NwLocationChangedListener;

/**
 * @author Thomas Schmittner
 * 
 */
public class ServiceProvider {

	private AccService accService;
	private LocService nwLocService;
	private GpsService gpsLocService;
	private Intent intentAccService;
	private Intent intentNwLocService;
	private Intent intentGpsLocService;
	private Context context;
	private MiddlewareIntent activity;

	private static ServiceProvider instance;

	public static ServiceProvider instance() throws NotInitializedException {
		if (instance == null) {
			throw new NotInitializedException();
		}
		return instance;
	}

	public ServiceProvider(Context context, MiddlewareIntent middlewareIntent) {
		this.context = context;
		this.activity = middlewareIntent;
		instance = this;
	}

	private ServiceConnection accServiceConnection;

	public void startAccelerometerService(final AccelerometerDataListener l) {
		accServiceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className,
					IBinder service) {
				accService = (AccService) ((AccService.LocalBinder) service)
						.getService();
				accService.registerAccelerometerDataListener(l);
			}

			public void onServiceDisconnected(ComponentName className) {
				// TODO Auto-generated method stub
			}
		};
		intentAccService = new Intent(context, AccService.class);
		context.startService(intentAccService);
		doBindService(AccService.class, accServiceConnection);
	}

	public void stopAccelerometerService(AccelerometerDataListener l) {
		accService.unregisterAccelerometerDataListener(l);
		context.unbindService(accServiceConnection);
		context.stopService(intentAccService);
	}

	private ServiceConnection nwLocServiceConnection;

	public void startNwLocationService(final NwLocationChangedListener l) {
		nwLocServiceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className,
					IBinder service) {
				nwLocService = (LocService) ((LocService.LocalBinder) service)
						.getService();
				nwLocService.registerLocationChangedListener(l);
			}

			public void onServiceDisconnected(ComponentName className) {
				// TODO Auto-generated method stub
			}
		};
		intentNwLocService = new Intent(context, LocService.class);
		context.startService(intentNwLocService);
		doBindService(LocService.class, nwLocServiceConnection);
	}

	public void stopNwLocationService(NwLocationChangedListener l) {
		nwLocService.unregisterLocationChangedListener(l);
		context.unbindService(nwLocServiceConnection);
		context.stopService(intentNwLocService);
	}

	private ServiceConnection gpsLocServiceConnection;

	public void startGpsLocationService(final GpsLocationChangedListener l) {
		gpsLocServiceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName className,
					IBinder service) {
				gpsLocService = (GpsService) ((GpsService.LocalBinder) service)
						.getService();
				gpsLocService.registerLocationChangedListener(l);
			}

			public void onServiceDisconnected(ComponentName className) {
				// TODO Auto-generated method stub
			}
		};
		intentGpsLocService = new Intent(context, GpsService.class);
		context.startService(intentGpsLocService);
		doBindService(GpsService.class, gpsLocServiceConnection);
	}

	public void stopGpsLocationService(GpsLocationChangedListener l) {
		gpsLocService.unregisterLocationChangedListener(l);
		context.unbindService(gpsLocServiceConnection);
		context.stopService(intentGpsLocService);
	}

	private void doBindService(Class<?> cls, ServiceConnection serviceConnection) {
		context.bindService(new Intent(activity, cls), serviceConnection,
				Context.BIND_AUTO_CREATE);
	}
}