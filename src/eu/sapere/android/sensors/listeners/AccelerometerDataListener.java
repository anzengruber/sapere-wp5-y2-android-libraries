/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.android.sensors.listeners;

import android.hardware.SensorEvent;

/**
 * An {@linkplain AccelerometerDataListener} provides an interface for being
 * notified whenever the accelerometer data collected from the built-in sensor
 * of a phone change.
 * 
 * @author Thomas Schmittner
 * 
 */
public interface AccelerometerDataListener {

	/**
	 * This method is called whenever the accelerometer data change and needs to
	 * be implemented to trigger the intended action.
	 * 
	 * @param event
	 *            The {@linkplain SensorEvent} including the new data values.
	 */
	public void onAccDataChanged(SensorEvent event);
}