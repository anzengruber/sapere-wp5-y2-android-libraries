/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.android.sensors.listeners;

import android.location.Location;

/**
 * An {@linkplain GpsLocationChangedListener} provides an interface for being
 * notified whenever the location based on the built-in GPS receiver changes.
 * 
 * @author Thomas Schmittner
 * 
 */
public interface GpsLocationChangedListener {

	/**
	 * This method is called whenever the GPS location changes and needs to be
	 * implemented to trigger the intended action.
	 * 
	 * @param event
	 *            The {@linkplain Location} including the new data values.
	 */
	public void onGpsLocationChanged(Location location);
}