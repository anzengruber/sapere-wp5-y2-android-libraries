/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.application;

import android.location.Location;
import eu.sapere.android.sensors.NotInitializedException;
import eu.sapere.android.sensors.ServiceProvider;
import eu.sapere.android.sensors.listeners.NwLocationChangedListener;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

/**
 * @author Thomas Schmittner
 * 
 */
public class NwLocationService extends SapereAgent implements
		NwLocationChangedListener {

	private static final String NETWORK_LOCATION = "nw-location";

	public NwLocationService(String name) {
		super(name);
	}

	public void startService() {
		System.out.println("--- NwLocationService startService()");
		addProperty(new Property(NETWORK_LOCATION, "0", "0"));
		try {
			ServiceProvider.instance().startNwLocationService(this);
		} catch (NotInitializedException e) {
			e.printStackTrace();
		}
	}

	public void stopService() {
		try {
			ServiceProvider.instance().stopNwLocationService(this);
		} catch (NotInitializedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNwLocationChanged(Location location) {
		String lon = String.valueOf(location.getLongitude());
		String lat = String.valueOf(location.getLatitude());
		addProperty(new Property(NETWORK_LOCATION, lon, lat));
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub
	}
}