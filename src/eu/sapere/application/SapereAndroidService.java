/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.application;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.util.Log;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class SapereAndroidService extends SapereAgent implements NeighbourListener, ProfileListener {


private UserProfile userProfile;
private volatile Map<String, HashMap<String, String>> neighbours;

	public SapereAndroidService(String name) {
		
		super(name);
		NeighbourAnalyzer.addListener(this);
		userProfile = new UserProfile();
		userProfile.register(this);
		neighbours = new HashMap<String, HashMap<String, String>>();
		// TODO Auto-generated constructor stub
		
		Log.d("TestService", "TestService costruttore");
	}

	@Override
	public void setInitialLSA() {
		Log.d("TestService", "TestService startService");
		addProperty(new Property("Profile", "myProfile"));
		
		//addProperty(new Property("service", "ServiceName"),new Property("property", "p-value"));
		
//		addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_10"), 
//		new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperators.MAX.toString()),
//		new Property(PropertyName.FIELD_VALUE.toString(), "x"), 
//		new Property(PropertyName.SOURCE.toString(), "session_id"),
//		new Property(PropertyName.PREVIOUS.toString(), "PrivateDisplayB"), 
//		new Property(PropertyName.DESTINATION.toString(), "Public-Display-TS"),
//		new Property("x", "10"));
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	public void onNeighbourFound(String key, HashMap<String, String> data) {
		if(!neighbours.containsKey(key))
			sendProfile(key,data);
	
		neighbours.put(key, data);	
	}

	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if(neighbours.containsKey(key))
			neighbours.remove(key);
	}

	private void sendProfile(String key, HashMap<String, String> data) {
//		Lsa newLsa = new Lsa();
		String target = "";
		Iterator<Entry<String, String>> i = data.entrySet().iterator();
		while(i.hasNext()){
			Entry<String, String> e = i.next();
			if(e.getKey().equals("neighbour")) target = e.getValue();
		}
	
		for(Property tmpProperty : userProfile.getProfile()) {
//			newLsa.addProperty(tmpProperty);
			addProperty(tmpProperty);
		}

//		newLsa.addProperty(new Property(PropertyName.PREVIOUS.toString(), this.opMng.getSpaceName()),
//			new Property(PropertyName.DESTINATION.toString(), target),
//			new Property(PropertyName.DECAY.toString(), "10"),
//			new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
	
		addProperty(new Property(PropertyName.PREVIOUS.toString(), this.opMng.getSpaceName()),
				new Property(PropertyName.DESTINATION.toString(), target),
				new Property(PropertyName.DECAY.toString(), "10"),
				new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
	}

	@Override
	public void profileChanged() {
		Set<Entry<String, HashMap<String, String>>> entrySet = neighbours.entrySet();
		for(Iterator<Entry<String, HashMap<String, String>>> it = entrySet.iterator(); it.hasNext();) {
			Entry<String, HashMap<String, String>> entry = it.next();
			sendProfile(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		event.getLsa().removeProperty(PropertyName.DIFFUSION_OP.toString());
		event.getLsa().removeProperty(PropertyName.DECAY.toString());
		event.getLsa().removeProperty(PropertyName.PREVIOUS.toString());
		event.getLsa().removeProperty(PropertyName.DESTINATION.toString()); 
	}
}
