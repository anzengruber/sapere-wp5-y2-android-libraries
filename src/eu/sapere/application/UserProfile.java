/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.application;

import java.util.Iterator;
import java.util.Vector;

import eu.sapere.Model;
import eu.sapere.ModelChangedListener;
import eu.sapere.middleware.lsa.Property;

public class UserProfile implements ModelChangedListener {
	private Vector<Property> profile = new Vector<Property>();
	private Vector<ProfileListener> profileListeners = new Vector<ProfileListener>();
	
	public UserProfile() {
		Model.instance().addModelChangedListener(this);
		profile.add(new Property("music", "Nightwish"));
		profile.add(new Property("sport", "Rafting"));
	}
	
	public Vector<Property> getProfile() {
		return profile;
	}

	@Override
	public void onPropertyAdded(eu.sapere.Property p) {
		for(Iterator<Property> it = profile.iterator(); it.hasNext();) {
			Property tmpProp = it.next();
			if(tmpProp.getName().equals(p.getKey())) {
				Vector<String> propValues = tmpProp.getValue();
				propValues.add(p.getValue());
				tmpProp.setValue(propValues);
				fireProfileChanged();
				return;
			}
		}
		fireProfileChanged();
		profile.add(new Property(p.getKey(), p.getValue()));
	}

	@Override
	public void onPropertyRemoved(eu.sapere.Property p) {
		for(Iterator<Property> it = profile.iterator(); it.hasNext();) {
			Property tmpProp = it.next();
			
			if(tmpProp.getName().equals(p.getKey())) {
				Vector<String> propValues = tmpProp.getValue();
				for(int i = 0; i<propValues.size(); i++) {
					String deleteVal = propValues.elementAt(i);
					if(deleteVal.equals(p.getValue())) {
						propValues.remove(i);
						fireProfileChanged();
						return;
					}
				}
			}
		}
	}
	
	public void register(ProfileListener listener) {
		profileListeners.add(listener);
	}

	private void fireProfileChanged() {
		for(int i = 0; i< profileListeners.size(); i++) {
			profileListeners.elementAt(i).profileChanged();
		}
	}

	@Override
	public void modelChanged() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProfileSendRequest() {
		// TODO Auto-generated method stub
		
	}
	
	
}
