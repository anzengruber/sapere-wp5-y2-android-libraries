/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.application;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.Set;

import android.util.Log;
import eu.sapere.Model;
import eu.sapere.ModelChangedListener;
import eu.sapere.middleware.agent.DataLsa;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class UserProfileAgent extends SapereAgent implements NeighbourListener,
		ModelChangedListener {

	private static final String PROFILE = "Profile";
	private Model userProfileModel;
	private volatile Map<String, HashMap<String, String>> neighbors;

	public UserProfileAgent(String name) {
		super(name);
		NeighbourAnalyzer.addListener(this);
		userProfileModel = Model.instance();
		userProfileModel.addModelChangedListener(this);
		neighbors = new HashMap<String, HashMap<String, String>>();
		Log.d("UserProfileAgent", "UserProfileAgent started");
		Log.d("TestService", "TestService startService");
	}

	@Override
	public void onNeighbourFound(String key, HashMap<String, String> data) {
		if (!neighbors.containsKey(key)) {
			sendProfile(key, data);
		}
		neighbors.put(key, data);
	}

	@Override
	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if (neighbors.containsKey(key)) {
			neighbors.remove(key);
		}
	}

	private void sendProfile(String neighbor, HashMap<String, String> data) {
		System.out.println(this.getClass() + " => sendProfile(...) to "
				+ neighbor + " with LSA : " + Model.instance().getProperties());
		Lsa newLsa = new Lsa();
		String target = "";
		for (Entry<String, String> entry : data.entrySet()) {
			if (entry.getKey().equals("neighbour")) {
				target = entry.getValue();
			}
		}
		userProfileModel.updateVersion();
		for (String key : Model.instance().getProperties().keySet()) {
			newLsa.addProperty(new Property(key, Model.instance()
					.getProperties().get(key).toArray(new String[0])));
		}
		newLsa.addProperty(new Property(PropertyName.PREVIOUS.toString(),
				this.opMng.getSpaceName()), new Property(
				PropertyName.DESTINATION.toString(), target), new Property(
				PropertyName.DECAY.toString(), "10"), new Property(
				PropertyName.DIFFUSION_OP.toString(), "direct"));
		new DataLsa(newLsa);
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		Property contentChannel = event.getBondedLsa().getProperty("content-channel");
		Property contentIndex = event.getBondedLsa().getProperty("content-index");
		
		if (contentChannel != null && contentChannel.getValue() != null
				&& contentChannel.getValue().size() > 0) {
			String lastChannel = contentChannel.getValue().get(0);
			Vector<String> value = new Vector<String>();
			value.add(lastChannel);
			Model.instance().updateProperties(Model.CONTENT_CHANNEL, value);
		}
		if (contentIndex != null && contentIndex.getValue() != null
				&& contentIndex.getValue().size() > 0) {
			String lastIndex = contentIndex.getValue().get(0);
			Vector<String> value = new Vector<String>();
			value.add(lastIndex);
			Model.instance().updateProperties(Model.CONTENT_INDEX, value);
		}
		event.getBondedLsa().addProperty(new Property(PropertyName.DECAY.toString(), "1"));
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
		System.out.println("jfakldjfklasd�kj");
	}

	@Override
	public void modelChanged() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProfileSendRequest() {
		Set<Entry<String, HashMap<String, String>>> entrySet = neighbors
				.entrySet();
		System.out.println(this.getClass() + " neighbors: " + neighbors);
		for (Iterator<Entry<String, HashMap<String, String>>> it = entrySet
				.iterator(); it.hasNext();) {
			Entry<String, HashMap<String, String>> entry = it.next();
			sendProfile(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property(PROFILE, "myProfile"));
		addProperty(new Property("user", this.agentName));
		addSubDescription("User-Profile", new Property("content-channel", "*"), new Property("content-index", "*"));
	}

	@Override
	public void onPropertyAdded(eu.sapere.Property p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropertyRemoved(eu.sapere.Property p) {
		// TODO Auto-generated method stub
		
	}
}