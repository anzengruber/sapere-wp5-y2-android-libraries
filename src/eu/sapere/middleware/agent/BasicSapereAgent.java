/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Agent;

/** 
 * The implementation provided for Data LSA.
 * Once a Data LSA is injected in the Space, it is
 * no longer on control of the entity that injected it, i.e.,
 * the events that happens to the LSA are not reported
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public  class BasicSapereAgent extends Agent{
	
	/** The reference to the local node's Operation Manager */
	private OperationManager opMng = null;
	
	/** 
	 * Instantiates a Basic Sapere Agent
	 * @param agentName The name of the Agent
	 */
	public BasicSapereAgent(String agentName){
		super(agentName);
		this.opMng = NodeManager.instance().getOperationManager();
	}	

	/**
	 * Injects the given LSA in the local Lsa space
	 * @param lsa The LSA to be injected 
	 * @return The id of the injected LSA
	 */
	public Id injectOperation (Lsa lsa){
		Id lsaId = null;
	
		Operation op = new Operation().injectOperation((Lsa)lsa, getAgentName(),  this);
		lsaId = opMng.queueOperation(op);
		System.out.println("injected with id "+lsaId);
		return lsaId;
	}


	/** 
	 * Updates an LSA
	 * @param lsa The new LSA 
	 * @param lsaId The id of the LSA to be updated
	 */
	public void updateOperation (Lsa lsa, Id lsaId){
		Operation op = new Operation().updateOperation(lsa, lsaId, getAgentName());
		opMng.queueOperation(op);
	}	
	
	/**
	 * Removes the specified LSA from the local LSA space
	 * 
	 * @param lsa
	 */
	public void removeLsa(Lsa lsa){
		Operation op = new Operation().removeOperation(lsa.getId(), getAgentName());
		opMng.queueOperation(op);
	}
	

	
}
