/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Agent;

/** 
 * The implementation provided for Data LSA.
 * Once a Data LSA is injected in the Space, it is
 * no longer on control of the entity that injected it, i.e.,
 * the events that happens to the LSA are not reported
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public  class DataLsa extends Agent{
			
	/** 
	 * Injects the provided LSA into the local LSA space
	 * @param lsa LSa to be injected
	 */
	public DataLsa(ILsa lsa){
		
		super("DataLsa");
		OperationManager opMng = NodeManager.instance().getOperationManager();
		Operation op = new Operation().injectOperation((Lsa)lsa, agentName,  this);
		opMng.queueOperation(op);
	}
	
}
