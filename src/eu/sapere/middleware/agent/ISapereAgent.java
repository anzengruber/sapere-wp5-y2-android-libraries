/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

/**
 * Interface with methods triggered on Events
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface ISapereAgent {
	
	//public void onReadNotification(ReadEvent event);

	//public void onUpdateNotification(UpdateEvent event);

	//public void onLsaExpiredNotification(LsaExpiredEvent event);

	/**
	 * Called when a new Bond happens
	 * 
	 * @param event The BondAddedEvent
	 */
	public void onBondAddedNotification(BondAddedEvent event);

	/**
	 * @param event
	 */
	public void onBondRemovedNotification(BondRemovedEvent event);

	/**
	 * @param event
	 */
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event);

	//public void onDecayNotification(DecayEvent event);

	//public void onAggregationRemovedNotification(AggregationRemovedEvent event);
	
	public void onPropagationEvent (PropagationEvent event);


}
