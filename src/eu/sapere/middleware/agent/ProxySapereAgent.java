/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import eu.sapere.middleware.agent.remoteconnection.internal.ResolveIpAddress;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.autoupdate.PropertyValueEvent;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.Event;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.SapereEvent;

public class ProxySapereAgent extends SapereAgent implements Runnable{
	
	Socket socket ;
	ObjectOutputStream oos ;
	ObjectInputStream ois;
	
	private SapereAgentRNM callingAgent = null;
	private ResolveIpAddress agent = null;
	
	private String ip = null;
	
	private boolean isOn = false;
	private Thread t = null;
	
	private Queue<ILsa> lsaQueue = null; // FIFO Queues
	
	public ProxySapereAgent(String name, String nodeName, SapereAgentRNM callingAgent){
		super (name);
		
		this.callingAgent = callingAgent;
		this.lsaQueue = new ConcurrentLinkedQueue<ILsa>();
		
		getIpFromNodeName(nodeName);
	}
	
	public void getIpFromNodeName(String nodeName){
		 agent = new ResolveIpAddress("agentIp", this);
		 agent.setInitialLSA(nodeName);
	}
	
	public void emptyQueue(){
		Iterator<ILsa> iterator = lsaQueue.iterator();
		while(iterator.hasNext()){
			ILsa nextLsa = (ILsa) iterator.next();
			lsaQueue.poll();
			forward(nextLsa);
		}
	}
	
	public void setIp(String ip){
		this.ip = ip;
		openConnection(ip);
		
		if(! lsaQueue.isEmpty())
			emptyQueue();
	}
	
	public String getAgentName(){
		return agentName;
	}
	
	public void setLsa(Lsa lsa){
		this.lsa = lsa;
	}
	

	public void forward (){
		
		if(ip != null){
			//System.out.println("proxy to "+ip);
			sendData(lsa);}
		else
			lsaQueue.add(lsa);
	}
	
	private void forward (ILsa lsa){
		
			sendData(lsa);
	}

	public void onNotification(Event event){
		// other events are not managed at the moment.
		
		if (event.getClass().isAssignableFrom(BondAddedEvent.class)){
			BondAddedEvent bondAddedEvent = (BondAddedEvent) event;
			 callingAgent.onBondAddedNotification(bondAddedEvent);
		}
		if (event.getClass().isAssignableFrom(BondedLsaUpdateEvent.class)){
			BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
			callingAgent.onBondedLsaUpdateEventNotification(bondedLsaUpdateEvent);
		}
		if (event.getClass().isAssignableFrom(BondRemovedEvent.class)){
			BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
			callingAgent.onBondRemovedNotification(bondRemovedEvent);
		}
		
		
	}

	  
	  public void onBondAddedNotification(BondAddedEvent event) {
		  
	  }
	  
	  public void onBondRemovedNotification(BondRemovedEvent event) {
	  }
	  
	  public void onBondedLsaUpdateEventNotification (BondedLsaUpdateEvent event){
	  }
	  

		
		public void sendData(Object input){
	    	try{
	    			oos.writeObject(input);
					oos.reset();
					oos.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
	    	}
		
		//Socket Part
		public void openConnection(String ip){
			
			try {
			
				socket = new Socket(ip, 1234);
				oos = new ObjectOutputStream(socket.getOutputStream());
				ois = new ObjectInputStream(socket.getInputStream());
			
			} catch (UnknownHostException e) {
				 // TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
					     // TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			this.isOn = true;
			t = new Thread(this);
			t.start();
			
		}
		
		public void closeConnection() {
	    	
	    	try{
				isOn = false;		

				oos.writeObject("clientLeaving");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	if (oos != null){
	    		   try {
	    		    oos.close();
	    		   } catch (IOException e) {
	    		    // TODO Auto-generated catch block
	    		    e.printStackTrace();
	    		   }
	    		  }

	    	if (ois != null){
	    		   try {
	    		    ois.close();
	    		   } catch (IOException e) {
	    		    // TODO Auto-generated catch block
	    		    e.printStackTrace();
	    		   }
	    		  }
	    	
			if (socket != null){
	    		try {
	    			
	    			socket.close();
	    		}catch (IOException e) {
			       // TODO Auto-generated catch block
			       e.printStackTrace();
			    }
			}
			
	    }


		public void run() {
			Object input;
			
			try {
				try {
					while (isOn && ((input= ois.readObject())!= null )){
						SapereEvent event = (SapereEvent) input;
						onNotification(event);
						
						
					}
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
    }
			

		@Override
		public void PropertyValueAppendGenerated(PropertyValueEvent event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setInitialLSA() {
			// not used here
			
		}

		@Override
		public void onPropagationEvent(PropagationEvent event) {
			// TODO Auto-generated method stub
			
		}
	
}
