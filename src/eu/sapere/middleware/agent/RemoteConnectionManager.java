/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.agent.remoteconnection.Server;

/**
 * Act as a skeleton for a Remote Connection Manager, that is accepts incoming
 * active connections from remote nodes, instantiates a Sapere Agent in order to
 * manage the incoming LSA and forwards events to the remote node.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class RemoteConnectionManager implements Runnable {

	/** The name of the Remote Connection Manager */
	protected String agentName = null;

	/** The reference to the server that manages incoming connections */
	private Server server = null;

	/**
	 * Instantiates the Remote Connection Manager
	 * 
	 * @param name
	 *            The name for the Remote Connection Manager
	 */
	public RemoteConnectionManager() {

		this.agentName = "RemoteConnectionManager";

		server = new Server();
		new Thread(this).start();

	}

	public void run() {
		
		server.startServer();
		
	}

}
