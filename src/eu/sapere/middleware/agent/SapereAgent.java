/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.autoupdate.EventGenerator;
import eu.sapere.middleware.lsa.autoupdate.PropertyValueEvent;
import eu.sapere.middleware.lsa.autoupdate.PropertyValueListener;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;
import eu.sapere.middleware.lsa.values.AggregationOperators;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.NodeManager;

/**
 * The abstract class that realize an agent that manages an LSA. The Agent
 * is represented by an implicit LSA, each operation on the LSA is automatically
 * and transparently propagated to the LSA space. 
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class SapereAgent extends LsaAgent implements PropertyValueListener{



	/**
	 * Instantiates the Smart Sapere Agent
	 * @param name The name of the Agent
	 */
	public SapereAgent(String name) {
		super(name);
	}
	
	/**
	 * Use this method to set the initial content of the LSA managed by this SapereAgent.
	 */
	public abstract void setInitialLSA();

	
	/**
	 * Adds Properties to the LSA managed by this SapereAgent
	 * 
	 * @param properties The Properties to be added
	 */
	public void addProperty(Property... properties) {

		Property p[] = properties;
		for (int i = 0; i < p.length; i++) {
			lsa.addProperty(p[i]);
		}

		submitOperation();

	}

	/**
	 * Adds a Property to the LSA managed by this SapereAgent
	 * 
	 * @param p The Property to be added
	 */
	public void addProperty(Property p) {
		lsa.addProperty(p);
		submitOperation();
	}
	
	public void addDirectPropagation(String destinationNode){
		this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"),
				new Property(PropertyName.DESTINATION.toString(), destinationNode));
	}
	
	public void addDirectPropagation(){
		this.addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"),
				new Property(PropertyName.DESTINATION.toString(), "all"));
	}
	
	public void addGradient(int nHop, String aggregationOperator, String gradientFieldName){
		addProperty(new Property(PropertyName.DIFFUSION_OP.toString(), "GRADIENT_"+nHop), 
				new Property(PropertyName.AGGREGATION_OP.toString(), aggregationOperator),
				new Property(PropertyName.FIELD_VALUE.toString(), gradientFieldName), 
				new Property(PropertyName.SOURCE.toString(), this.agentName+"@"+NodeManager.instance().getSpaceName()),
				new Property(PropertyName.PREVIOUS.toString(), "local"), 
				new Property(PropertyName.DESTINATION.toString(), "default"),
				new Property(gradientFieldName, ""+nHop));
	}
	
	public void addOtherAggregation(String fieldName, String AggregationOperator){
		addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperator));
		addProperty(new Property(PropertyName.FIELD_VALUE.toString(), fieldName));
	}
	
	public void addSelfAggregation(String fieldName, String AggregationOperator, String aggregationSource){
		addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperator));
		addProperty(new Property(PropertyName.FIELD_VALUE.toString(), fieldName));
		addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
	}
	
	public void addSelfAggregationNEWEST(String aggregationSource){
		addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperators.MAX.toString()));
		addProperty(new Property(PropertyName.FIELD_VALUE.toString(), "#lastModified"));
		addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
	}
	
	public void addSelfAggregationOLDEST(String aggregationSource){
		addProperty(new Property(PropertyName.AGGREGATION_OP.toString(), AggregationOperators.MIN.toString()));
		addProperty(new Property(PropertyName.FIELD_VALUE.toString(), "#lastModified"));
		addProperty(new Property(PropertyName.SOURCE.toString(), aggregationSource));
	}
	
	public void addDecay(int decayValue){
		addProperty(new Property(PropertyName.DECAY.toString(), ""+decayValue));
	}

	/**
	 * Adds a Property that automatically updates
	 * 
	 * @param name The name of the Property
	 * @param eg The generator of automatically updates
	 */
	public void addProperty(String name, EventGenerator eg) {
		eg.setPropertyName(name);
		eg.addPropertyValueListener(this);
	}

	/**
	 * Sets the value of an existing Property
	 * @param p The new Property value 
	 */
	public void setProperty(Property p) {

		try {
			lsa.setProperty(p);
			submitOperation();
		} catch (UnresolvedPropertyNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Removes a Property from the managed LSA
	 * @param name
	 */
	public void removeProperty(String name) {
		lsa.removeProperty(name);
		submitOperation();
	}

	/**
	 * Adds a SubDescription with a single Property
	 * 
	 * @param name
	 * @param value
	 */
	public void addSubDescription(String name, Property value) {
		lsa.addSubDescription(name, value);
		submitOperation();
	}

	/**
	 * Adds a SubDescription with a variable number of Properties
	 * 
	 * @param name
	 * @param values
	 */
	public void addSubDescription(String name, Property... values) {
		//SubDescription s = null;
		lsa.addSubDescription(name, values);

		//s.toString();

		submitOperation();
	}

	/**
	 * Removes the specified SubDescription from the managed LSA
	 * 
	 * @param name
	 */
	public void removeSubDescription(String name) {
		lsa.removeSubDescription(name);
		submitOperation();
	}

	/**
	 *Stes the value of an existing SubDescription
	 * 
	 * @param name
	 * @param value
	 */
	public void setSubDescription(String name, Property value) {
		lsa.setSubDescription(name, value);
		submitOperation();
	}
	
	public void PropertyValueGenerated(PropertyValueEvent event) {
		if (!((Lsa) lsa).hasProperty(event.getPropertyName())) {
			addProperty(new Property(event.getPropertyName(), event.getValue()));
		} else {
			//update
			setProperty(new Property(event.getPropertyName(), event.getValue()));
		}
	}
	
	@Override
	public void PropertyValueAppendGenerated(PropertyValueEvent event) {
		if (!((Lsa) lsa).hasProperty(event.getPropertyName())) {
			addProperty(new Property(event.getPropertyName(), event.getValue()));
		} else {
			//update
			//setProperty(new Property(event.getPropertyName(), event.getValue()));
			Property p = lsa.getProperty(event.getPropertyName());
			p.getPropertyValue().addValue(event.getValue());
			setProperty(new Property(event.getPropertyName(), p.getValue()));

		}
		
	}

}
