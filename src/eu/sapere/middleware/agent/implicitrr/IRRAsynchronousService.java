/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.implicitrr;

import eu.sapere.middleware.lsa.Lsa;

/**
 * Interface for Implicit Request-Response Services.
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface IRRAsynchronousService {
	
	/**
	 * Called on bond. It is up to the programmer to provide the service answer in the property with "!" value. 
	 * 
	 * @param bondedLsa
	 * @return
	 */
	public void onBondedLsaNotification (Lsa bondedLsa);
	
	

}

