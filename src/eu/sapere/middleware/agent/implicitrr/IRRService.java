/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.implicitrr;

import java.util.Vector;

import eu.sapere.middleware.lsa.Lsa;

/**
 * Interface for Implicit Request-Response Services.
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface IRRService {
	
	/**
	 * Called on bond. This method is used to provide the service.
	 * 
	 * @param bondedLsa
	 * @return
	 */
	public Vector<String> provideResponse (Lsa bondedLsa );

}
