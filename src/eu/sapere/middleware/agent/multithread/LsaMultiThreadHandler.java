/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.multithread;

import eu.sapere.middleware.lsa.interfaces.ILsa;

/**
 * The class to be implemented to handle a multi-thread LSA
 * as a reaction to events happened (bonds, modification in a bonded LSA, etc.)
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class LsaMultiThreadHandler implements ILsaHandler{
	
	/**
	 * Returns a new Thread Handler
	 * 
	 * @return a new Handler
	 *
	abstract LsaMultiThreadHandler getHandler();  */
	
	public abstract void onBond(ILsa bondedLsa, ThreadSapereAgent callback);
	
	public abstract void onBondedLsaModified(ILsa bondedLsa, ThreadSapereAgent callback);
	
	public abstract LsaMultiThreadHandler getHandler(); 
	
	/*
	public LsaMultiThreadHandler getHandler() {
		return new LsaMultiThreadHandler();
	}*/

	/*
	@Override
	public void onBond(ILsa bondedLsa, ThreadSapereAgent callback){
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBondedLsaModified(ILsa bondedLsa, ThreadSapereAgent callback) {
		// TODO Auto-generated method stub
		
	}*/

}
