/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.multithread;

import eu.sapere.middleware.agent.MultithreadSapereAgent;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.autoupdate.PropertyValueEvent;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

/**
 * The implementation of a multi-thread Agent
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class ThreadSapereAgent extends SapereAgent {
	
	protected MultithreadSapereAgent parent = null;

	private ILsaHandler handler = null;
	
	private Long id = null;

	/**
	 * Instantiates a Thread Agent
	 * @param name 
	 * 
	 * @param opMng The reference to the Operation Manager
	 * @param notifier The reference to the Notifier
	 * @param parent The reference to the Parent Sapere Agent
	 * @param handler The handler for events that happens to the Thread Agent
	 */
	public ThreadSapereAgent(String name, OperationManager opMng, INotifier notifier,
			MultithreadSapereAgent parent, ILsaHandler handler) {
		
		super(name);
		
		this.opMng = opMng;
		this.notifier = notifier;
		this.parent = parent;
		this.handler = handler;
	}
	
	/**
	 * Sets the id for the Thread Agent
	 * 
	 * @param id The id of the Thread
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return
	 */
	public Long getId(){
		return id;
	}

	/**
	 * The initial LSA
	 * 
	 * @param startingLsa The initial LSA
	 */
	public void setInitialLSA(ILsa startingLsa) {
			
		this.lsa = (Lsa) startingLsa;

		this.submitOperation();
	}
	
	@Override
	public void setInitialLSA() { //not used
	}
	
	
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		
		((LsaMultiThreadHandler) handler).onBond((Lsa) event.getBondedLsa(), this);

		parent.thread();
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		this.removeLsa();
		parent.removeThread(this);
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		handler.onBondedLsaModified(event.getLsa(), this);
	}

	@Override
	public void PropertyValueAppendGenerated(PropertyValueEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
		
	}

}
