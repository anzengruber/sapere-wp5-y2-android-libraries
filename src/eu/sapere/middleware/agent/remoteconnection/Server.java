/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.remoteconnection;

import java.io.*;
import java.net.*;

/**
 * The server waiting incoming connection for the Remote Connection Manager
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Server {
	
	/** The Server Scoket */
	static ServerSocket ss = null;
	
	/** Used to close the server */
	static boolean active=false;

	/**
	 * Starts the server
	 */
	public  void startServer(){
		
		//System.out.println ("Running Remote Connection Server Server...");

		
		active=true;

		try{

			ss = new ServerSocket (1234); 
	
			while (active){ //Server rimane in attesa 
	
				Socket clientsocket = ss.accept();// Attesa socket
		
				new ThreadServer (clientsocket).start();
			
			}
		}catch (Exception e){
			//System.out.println("Errore di apertura porta");

		}
	}
	
	/**
	 * Stops the server
	 */
	static void stopServer(){
		active=false;
		try {
			ss.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}