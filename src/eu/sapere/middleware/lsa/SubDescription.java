/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa;

import java.util.Map;

import eu.sapere.middleware.lsa.values.SyntheticPropertyName;

/**
 * 
 * Represents a SubDescription 
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class SubDescription extends Description{
	
	public SubDescription(Id id, Property p) {
		super(id, p);
		// TODO Auto-generated constructor stub
	}

	public SubDescription(Id id, Property[] properties) {
		super(id, properties);
		// TODO Auto-generated constructor stub
	}
	
	protected SubDescription(Id id, Map<String, Property> Properties, Map<SyntheticPropertyName, Property> SyntheticProperties) {
		super(id, Properties, SyntheticProperties);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 7319728270982187084L;

	@Override
	public SubDescription getCopy(){
		Description d = super.getCopy();
		SubDescription sd = new SubDescription(d.getId().getCopy(), d.Properties, d.SyntheticProperties);
		return sd;
	}

}
