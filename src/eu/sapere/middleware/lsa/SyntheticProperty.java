/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa;


import java.io.Serializable;

import eu.sapere.middleware.lsa.values.SyntheticPropertyName;





/**
 *Represets a Synthetic Property, i.e., a Property added and maintained by the middleware. 
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class SyntheticProperty extends Property implements Serializable, Cloneable {
	
	//CREATOR_ID , CREATION_TIME, LAST_MODIFIED, LOCATION, BONDS;
	
	private static final long serialVersionUID = -4698475527922276434L;

	
	/**
	 * Initializes a Synthetic Property with the specified name
	 * @param name The name of the Property
	 */
	public SyntheticProperty (SyntheticPropertyName name){
		super(name.toString());
		}
	
	/**
	 * Initializes a Synthetic Property with the specified name and a single value
	 * @param name The name of the Property
	 * @param value The value of the Property
	 */
	public SyntheticProperty (SyntheticPropertyName name, String value){
		super(name.toString(), value);
	}
	
	/**
	 * Initializes a Synthetic Property with the specified name and a multiple value
	 * @param name The name of the Property
	 * @param values The values of the Property
	 */
	public SyntheticProperty (SyntheticPropertyName name, String... values){
		
		super(name.toString(), values);
		}
	
	
	

}
