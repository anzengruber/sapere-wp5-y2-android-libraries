/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.lsa.autoupdate;

import java.util.EventObject;

// The event
public class PropertyValueEvent extends EventObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5461269044381723264L;
	private String propertyName;
    private String value;
    
    public PropertyValueEvent( Object source, String propertyName, String value ) {
        super( source );
        this.propertyName = propertyName;
        this.value = value;
    }
    
    public String getPropertyName() {
        return propertyName;
    }
    
    public String getValue() {
        return value;
    }
    
}
