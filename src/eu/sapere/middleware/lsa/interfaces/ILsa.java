/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.lsa.interfaces;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SubDescription;
import eu.sapere.middleware.lsa.exception.MalformedDescriptionException;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;

/**
 * 
 */





/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public interface ILsa {
	
	public Lsa addProperty(Property p) throws MalformedDescriptionException;
	public Lsa addProperty(Property ...properties); 							
	public Lsa removeProperty (String propertyName);
	public Lsa setProperty(Property p) throws UnresolvedPropertyNameException;
	public Property getProperty (String name);
	
	public SubDescription addSubDescription(String name, Property value);
	public SubDescription addSubDescription(String name, Property... values);  
	public Lsa removeSubDescription(String name);
	public ISubDescription setSubDescription(String name, Property value);
	
	public String toString();

}
