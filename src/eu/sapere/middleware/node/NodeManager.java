/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node;

import android.util.Log;
import eu.sapere.application.AccelerometerService;
import eu.sapere.application.GpsLocationService;
import eu.sapere.application.NwLocationService;
import eu.sapere.application.UserProfileAgent;
import eu.sapere.middleware.node.lsaspace.EcoLawsEngine;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.node.networking.delivery.INetworkDeliveryManager;
import eu.sapere.middleware.node.networking.delivery.NetworkDeliveryManager;
import eu.sapere.middleware.node.networking.physical.bluetooth.android.NeighborDiscoveryThread;
import eu.sapere.middleware.node.networking.receiver.NetworkReceiverManager;
import eu.sapere.middleware.node.networking.topology.NetworkTopologyManager;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.Notifier;

/**
 * Initializes the Local Sapere Node
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class NodeManager {

	/** The Operation Manager */
	private OperationManager operationManager = null;

	/** The eco-laws engine */
	private EcoLawsEngine ecoLawsEngine = null;

	/** The Network Delivery Manager */
	private INetworkDeliveryManager networkDeliveryManager = null;

	/** The Network Receiver Manger */
	private NetworkReceiverManager networkReceiverManager = null;

	/** The Network Topology Manager */
	private NetworkTopologyManager networkTopologyManager = null;

	/** The local LSA space */
	private Space space = null;

	/** The local Notifier */
	private Notifier notifier = null;

	/** The name of the local Node */
	private String spaceName = null;

	private static NodeManager singleton = null;

	private UserProfileAgent userProfileAgent = null;

	private NwLocationService nwLocService;
	private AccelerometerService accService;
	private GpsLocationService gpsLocService;

	/**
	 * Returns the name of this Node
	 * 
	 * @return The Name of this Node
	 */
	public String getSpaceName() {
		return spaceName;
	}

	/**
	 * This is the only way to access the singleton instance.
	 * 
	 * @return A reference to the singleton.
	 */
	public static NodeManager instance() {
		if (singleton == null) {
			singleton = new NodeManager();
		}
		return singleton;
	}

	/**
	 * Starts the Node Manager
	 * 
	 * @param name
	 */
	public void startNode(String name) {
		spaceName = name;
		this.notifier = Notifier.instance();

		space = new Space(spaceName, notifier);
		this.networkDeliveryManager = new NetworkDeliveryManager();

		operationManager = new OperationManager(space, notifier, spaceName);
		ecoLawsEngine = new EcoLawsEngine(space);

		operationManager.setEcoLawsEngine(ecoLawsEngine);
		ecoLawsEngine.setOperationManager(operationManager);
		ecoLawsEngine.setNetworkDeliveryManager(networkDeliveryManager);

		new Thread(operationManager).start();
		new Thread(ecoLawsEngine).start();

		NeighborDiscoveryThread neighborDiscoveryThread = new NeighborDiscoveryThread();
		neighborDiscoveryThread.start();

		this.networkReceiverManager = new NetworkReceiverManager();
		this.networkTopologyManager = new NetworkTopologyManager(neighborDiscoveryThread);
		this.networkTopologyManager.initializeNetworkTopologyManager();

		// Run the application
		userProfileAgent = new UserProfileAgent("userProfileAgent");
		userProfileAgent.setInitialLSA();

		// Start the CustomerInformationAgent
		// new CustomerInformationAgent("customerInformationAgent")
		// .setInitialLSA();

		accService = new AccelerometerService("accService");
		accService.startService();
		//
		// nwLocService = new NwLocationService("locService");
		// nwLocService.startService();
		//
		// gpsLocService = new GpsLocationService("gpsService");
		// gpsLocService.startService();

	}

	/**
	 * Retrieves the local Operation Manager
	 * 
	 * @return the local Operation Manager
	 */
	public OperationManager getOperationManager() {
		return operationManager;
	}

	/**
	 * Retrieves the local Notifier
	 * 
	 * @return the local Notifier
	 */
	public INotifier getNotifier() {
		return notifier;
	}

	public void stopServices() {
		Log.d("Quit", "SpaceManager stopping services...! ");
		networkTopologyManager.stopNetworkTopologyManager();
		networkReceiverManager.stopNetworkReceiverManager();
		ecoLawsEngine.stopEcoLawsEngine();
		operationManager.stopOperationManager();
		accService.stopService();
		nwLocService.stopService();
		gpsLocService.stopService();
	}

	public Space getSpace() {
		return space;
	}
}