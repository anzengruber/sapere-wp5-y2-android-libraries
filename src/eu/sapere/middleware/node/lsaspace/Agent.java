/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.lsaspace;

import eu.sapere.middleware.node.notifier.Subscriber;
import eu.sapere.middleware.node.notifier.event.Event;

/**
 * Abstract class that represents a Sapere Agent
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public abstract class Agent extends Subscriber {

	protected String agentName = null;
	
	/**
	 * @param agentName The name of this Agent
	 */
	public Agent(String agentName){
		this.agentName = agentName;
	}
	

	/**
	 * Retrieves the name of the Agent
	 * 
	 * @return The name of the Agent
	 */
	public String getAgentName() {
		return agentName;
	}

	@Override
	public void onNotification(Event event) {
		// TODO Auto-generated method stub
	}

}
