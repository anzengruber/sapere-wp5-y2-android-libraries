/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import eu.sapere.MiddlewareIntent;
import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.SubDescription;
import eu.sapere.middleware.lsa.SyntheticProperty;
import eu.sapere.middleware.lsa.values.SyntheticPropertyName;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.event.AggregationRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.DecayEvent;
import eu.sapere.middleware.node.notifier.event.LsaExpiredEvent;
import eu.sapere.middleware.node.notifier.event.ReadEvent;
import eu.sapere.middleware.node.notifier.event.SapereEvent;

/**
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class Space {

	private HashMap<String, Lsa> space = null;
	private String name = null;
	private static long nextId = 0L;

	private INotifier notifier = null;

	private MiddlewareIntent console = null;

	public Space(String name, INotifier notifier) {
		space = new HashMap<String, Lsa>();
		this.name = name;
		this.notifier = notifier;
		this.console = MiddlewareIntent.getInstance();

	}

	private void updateConsole() {
		if (console != null) {
			// FIXME remove comment if needed
			// Log.d("restart", "console != null");
			console.update(getAllLsa());
		}
		// FIXME remove comment if needed
		// Log.d("restart", "console == null");
	}

	/* inject the copy of an LSA into the LSA-space */
	public void inject(Lsa lsa, String creatorId) {
		Lsa copy = lsa.getCopy();

		// add Synthetic Properties
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.CREATOR_ID, new String(creatorId)));
		String currentTime = "" + new Date().getTime();
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.CREATION_TIME, new String(currentTime)));
		copy.addProperty(new SyntheticProperty(
				SyntheticPropertyName.LAST_MODIFIED, new String(currentTime)));
		copy.addProperty(new SyntheticProperty(SyntheticPropertyName.LOCATION,
				"local"));

		space.put(lsa.getId().toString(), copy);
		updateConsole();
	}

	public void update(Id lsaId, Lsa lsa, String creatorId) {
		update(lsaId, lsa, creatorId, true, true);
	}

	public void update(Id lsaId, Lsa lsa, String creatorId,
			boolean destroyBonds, boolean generateEvents) {

		Lsa copy = lsa.getCopy();
		Lsa oldLsa = space.get(lsaId.toString());

		if (space.containsKey(lsaId.toString()))
			if (space.get(lsaId.toString()).hasSyntheticProperty(
					SyntheticPropertyName.CREATOR_ID))
				if (space.get(lsaId.toString())
						.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
						.getValue().elementAt(0).equals(creatorId)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)) {

					copy.setId(new Id(lsaId.toString()));

					String currentTime = "" + new Date().getTime();
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.CREATOR_ID, new String(space
									.get(lsaId.toString())
									.getSyntheticProperty(
											SyntheticPropertyName.CREATOR_ID)
									.getValue().elementAt(0))));
					copy.addProperty((SyntheticProperty) oldLsa
							.getSyntheticProperty(SyntheticPropertyName.CREATION_TIME));
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.LAST_MODIFIED, new String(
									currentTime)));
					copy.addProperty(new SyntheticProperty(
							SyntheticPropertyName.LOCATION, "local"));
					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)) {
						if (lsa.hasBonds())
							for (int i = 0; i < lsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().size(); i++)
								copy.addBond(lsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));
					}

					else {
						if (oldLsa.hasBonds())
							for (int i = 0; i < oldLsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().size(); i++)
								copy.addBond(oldLsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));

						SubDescription sd[] = copy.getSubDescriptions();
						if (sd != null)
							for (int i = 0; i < sd.length; i++) {
								SubDescription oldSd = oldLsa
										.getSubDescriptionById(sd[i].getId()
												.toString());
								if (oldSd != null)
									if (oldSd.hasBonds()) {
										Vector<String> oldBonds = oldSd
												.getSyntheticProperty(
														SyntheticPropertyName.BONDS)
												.getValue();
										for (int j = 0; j < oldBonds.size(); j++)
											sd[i].addBond(oldBonds.elementAt(j));

									}
							}
					}

					// System.out.println("\t*\t"+copy);
					space.put(copy.getId().toString(), copy);

					if (destroyBonds)
						destroyBonds(this.getAllLsa(), this);

					if (generateEvents) {
						SapereEvent event = new BondedLsaUpdateEvent(copy);
						event.setRequiringAgent(null);
						notifier.publish(event);
					}

					String subjectId = copy.getId().toString();

					// if(destroyBonds)
					// destroyBonds(this.getAllLsa(), this);

					// Update the lsa
					lsa = space.get(subjectId);

					// Generate Events
					if (lsa.hasBonds())

						for (int i = 0; i < lsa
								.getSyntheticProperty(
										SyntheticPropertyName.BONDS).getValue()
								.size(); i++) {

							if (!oldLsa.hasBondWith(new Id(lsa
									.getSyntheticProperty(
											SyntheticPropertyName.BONDS)
									.getValue().elementAt(i)))) {
								// System.out.println("genero evento "+i);

								// MODIFICATO QUI
								// Lsa bondedLsa =
								// space.get(lsa.getSyntheticProperty(SyntheticPropertyName.BONDS).getValue().elementAt(i));
								// System.out.println("\t"+lsa.getSyntheticProperty(SyntheticPropertyName.BONDS).getValue().elementAt(i));
								// System.out.println(bondedLsa);
								// System.out.println(this.getLsa(lsa.getSyntheticProperty(SyntheticPropertyName.BONDS).getValue().elementAt(i)));

								Lsa bondedLsa = getLsa(lsa
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue().elementAt(i));
								SapereEvent event = new BondAddedEvent(
										copy.getCopy(),
										lsa.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
												.getValue().elementAt(i),
										bondedLsa);
								event.setRequiringAgent(new String(
										space.get(lsaId.toString())
												.getSyntheticProperty(
														SyntheticPropertyName.CREATOR_ID)
												.getValue().elementAt(0)));

								notifier.publish(event);

							}
						} // end for

					if (lsa.hasSubDescriptions()) {
						SubDescription sd[] = lsa.getSubDescriptions();
						for (int i = 0; i < sd.length; i++) {
							if (sd[i].hasBonds()) {
								Vector<String> bonds = sd[i]
										.getSyntheticProperty(
												SyntheticPropertyName.BONDS)
										.getValue();
								for (int j = 0; j < bonds.size(); j++) {
									if (!oldLsa.hasBondWith(new Id(bonds
											.elementAt(j)))) {

										Lsa bondedLsa = getLsa(bonds
												.elementAt(j));

										SapereEvent event = new BondAddedEvent(
												copy.getCopy(),
												bonds.elementAt(j), bondedLsa);
										event.setRequiringAgent(new String(
												space.get(lsaId.toString())
														.getSyntheticProperty(
																SyntheticPropertyName.CREATOR_ID)
														.getValue()
														.elementAt(0)));
										notifier.publish(event);
									}

								}
							}
						}
					}

				}

		if (creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)) {
			SapereEvent decayEvent = new DecayEvent(copy.getCopy());
			decayEvent.setRequiringAgent(null);
			notifier.publish(decayEvent);
		}

		updateConsole();
	}

	public void remove(Id lsaId, String creatorId) {

		if (space.containsKey(lsaId.toString()))
			if (space.get(lsaId.toString()).hasSyntheticProperty(
					SyntheticPropertyName.CREATOR_ID))
				if (space.get(lsaId.toString())
						.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
						.getValue().elementAt(0).equals(creatorId)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGENT)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_AGGREGATION)
						|| creatorId.equals(EcoLawsEngine.ECO_LAWS_PROPAGATION)) {

					Lsa oldLsa = space.get(lsaId.toString());
					space.remove(lsaId.toString());

					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_DECAY)) {
						// Fire event LsaExpired
						SapereEvent event = new LsaExpiredEvent(oldLsa);
						event.setRequiringAgent(oldLsa
								.getSyntheticProperty(
										SyntheticPropertyName.CREATOR_ID)
								.getValue().elementAt(0).toString());
						notifier.publish(event);
					}

					if (creatorId.equals(EcoLawsEngine.ECO_LAWS_AGGREGATION)) {

						SapereEvent event = new AggregationRemovedEvent(oldLsa);
						event.setRequiringAgent(oldLsa
								.getSyntheticProperty(
										SyntheticPropertyName.CREATOR_ID)
								.getValue().elementAt(0).toString());

						notifier.publish(event);
					}
				}

		destroyBonds(this.getAllLsa(), this);
		updateConsole();
	}

	public void read(Id targetId, String requestingId) {
		Lsa ret = null;

		if (space.containsKey(targetId.toString())) { // se esiste LSA da
														// chiedere
			Lsa target = space.get(targetId.toString());
			String targetCreator = target
					.getSyntheticProperty(SyntheticPropertyName.CREATOR_ID)
					.getValue().firstElement();
			if (targetCreator.equals(requestingId)) {
				ret = target.getCopy();
			}

			else {

				Iterator<String> iterator = space.keySet().iterator();
				while (iterator.hasNext()) {
					String key = (String) iterator.next();
					Lsa lsa = space.get(key);
					String creatorId = lsa
							.getSyntheticProperty(
									SyntheticPropertyName.CREATOR_ID)
							.getValue().firstElement();

					if (creatorId.equals(requestingId))
						if (lsa.hasBondWith(targetId)) {
							ret = target.getCopy();
							break;
						}

				}

			}

		}

		if (ret != null) {
			SapereEvent event = new ReadEvent(ret);
			event.setRequiringAgent(requestingId);
			notifier.publish(event);
		}

		updateConsole();
	}

	public Lsa getLsa(String key) {
		Id id = new Id(key);
		Lsa ret = null;

		if (!Id.isSdId(id))
			ret = space.get(key.substring(0, key.lastIndexOf("#")));
		else
			ret = space.get(key);
		return ret;
	}

	public Id getFreshId() {
		while (space.containsKey(name + "#" + nextId)) {
			nextId++;
		}
		return new Id(name + "#" + nextId++).getCopy();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {

		return space.values().toString();
	}

	public Lsa[] getAllLsa() {
		Lsa[] array = new Lsa[space.values().size()];

		return space.values().toArray(array);
	}

	public void print() {
		Lsa[] lsa = this.getAllLsa();

		for (int i = 0; i < lsa.length; i++) {
			System.out.println("\t" + lsa[i].toString());
		}

	}

	private static boolean isLsaId(String s) {
		if (s.indexOf("#") != s.lastIndexOf("#"))
			return false;
		return true;
	}

	private void destroyBonds(Lsa[] allLsaX, Space s) {

		Lsa[] allLsa = s.getAllLsa();

		for (int i = 0; i < allLsa.length; i++) {
			if (allLsa[i].hasBonds()) {

				Vector<String> activeBonds = allLsa[i].getSyntheticProperty(
						SyntheticPropertyName.BONDS).getValue();
				Vector<String> removeBonds = new Vector<String>(); // Bonds to
																	// be
																	// removed
				for (int j = 0; j < activeBonds.size(); j++) {

					if (isLsaId(activeBonds.elementAt(j))) {

						if (!s.space.containsKey(activeBonds.elementAt(j))) {
							removeBonds.add(activeBonds.elementAt(j));
						} else {

							if (!allLsa[i].matches(s.getLsa(activeBonds
									.elementAt(j))))
								removeBonds.add(activeBonds.elementAt(j));

						}
					} else {
						String LsaId = activeBonds.elementAt(j).substring(0,
								activeBonds.elementAt(j).lastIndexOf("#"));
						if (s.getLsa(LsaId) == null) {
							removeBonds.add(activeBonds.elementAt(j));
						} else

						if (!allLsa[i]
								.matches(s.getLsa(LsaId).getSubDescriptionById(
										activeBonds.elementAt(j))))
							removeBonds.add(activeBonds.elementAt(j));
					}

				}

				for (int k = 0; k < removeBonds.size(); k++) {
					allLsa[i].removeBond(removeBonds.elementAt(k));

					SapereEvent event = new BondRemovedEvent(
							allLsa[i].getCopy(), removeBonds.elementAt(k));
					event.setRequiringAgent(allLsa[i]
							.getCopy()
							.getSyntheticProperty(
									SyntheticPropertyName.CREATOR_ID)
							.getValue().elementAt(0));
					notifier.publish(event);

					// System.out.println("remove bonds to "+removeBonds.elementAt(k)+"\n\t in LSA "+allLsa[i]);

					// try to remove in the other
					Lsa exBondedLsa = s.getLsa(removeBonds.elementAt(k));
					// System.out.println(removeBonds.elementAt(k));
					// System.out.println(exBondedLsa);
					if (exBondedLsa != null)
						if (exBondedLsa.hasBondWith(allLsa[i].getId())) {

							// System.out.println("removed in the other");
							exBondedLsa
									.removeBond(allLsa[i].getId().toString());

							SapereEvent event2 = new BondRemovedEvent(
									exBondedLsa.getCopy(), allLsa[i].getId()
											.toString());
							event2.setRequiringAgent(exBondedLsa
									.getCopy()
									.getSyntheticProperty(
											SyntheticPropertyName.CREATOR_ID)
									.getValue().elementAt(0));
							notifier.publish(event2);

							// System.out.println("remove bonds to "+allLsa[i].getId()+"\n\t in LSA "+exBondedLsa.getCopy());

						}

				}

			}

			// TO DO: check events
			if (allLsa[i].hasSubDescriptions()) {
				SubDescription sd[] = allLsa[i].getSubDescriptions();
				for (int c = 0; c < sd.length; c++) {
					if (sd[c].hasBonds()) {
						Vector<String> activeBonds = sd[c]
								.getSyntheticProperty(
										SyntheticPropertyName.BONDS).getValue();
						Vector<String> removeBonds = new Vector<String>(); // Bonds
																			// to
																			// be
																			// removed
						for (int j = 0; j < activeBonds.size(); j++) {

							if (isLsaId(activeBonds.elementAt(j))) {

								if (!sd[c].matches(s.getLsa(activeBonds
										.elementAt(j))))
									removeBonds.add(activeBonds.elementAt(j));
							} else {
								String LsaId = activeBonds.elementAt(j)
										.substring(
												0,
												activeBonds.elementAt(j)
														.lastIndexOf("#"));
								if (!sd[c].matches(s.getLsa(LsaId)
										.getSubDescriptionById(
												activeBonds.elementAt(j))))
									removeBonds.add(activeBonds.elementAt(j));
							}

						}

						for (int k = 0; k < removeBonds.size(); k++) {

							sd[c].removeBond(removeBonds.elementAt(k));

							SapereEvent event = new BondRemovedEvent(
									allLsa[i].getCopy(),
									removeBonds.elementAt(k));
							event.setRequiringAgent(allLsa[i]
									.getCopy()
									.getSyntheticProperty(
											SyntheticPropertyName.CREATOR_ID)
									.getValue().elementAt(0));
							notifier.publish(event);

							System.out.println("remove bonds to "
									+ removeBonds.elementAt(k) + "\n\t in LSA "
									+ allLsa[i]);
						}

					}

				}
			}

		}

	}
}
