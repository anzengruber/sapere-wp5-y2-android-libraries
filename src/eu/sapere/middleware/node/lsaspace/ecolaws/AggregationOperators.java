/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.lsaspace.ecolaws;

import java.util.Date;
import java.util.Vector;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticProperty;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;
import eu.sapere.middleware.lsa.values.SyntheticPropertyName;



public enum AggregationOperators {
	
	NEWEST {
		public String toString() {
	        return "NEWEST";
	    }
	},
	
	OLDEST{
		public String toString() {
	        return "OLDEST";
	    }
	},
	
	MAX {
		public String toString() {
	        return "MAX";
	    }
	}, 
	
	MIN {
		public String toString() {
	        return "MIN";
	    }
	},
	
	AVG {
		public String toString() {
	        return "AVG";
	    }
	};
	
	
	private static long getValue(Lsa l, String fieldName){
		
		if(SyntheticPropertyName.isSyntheticProperty(fieldName)){
			return new Long (l.getSyntheticProperty(SyntheticPropertyName.getSyntheticPropertiesValues(fieldName)).getValue().elementAt(0)).longValue();
		
		}else
			return new Long (l.getProperty(l.getFieldValue()).getValue().elementAt(0)).longValue();
	}
	
	public static Id max (Vector<Lsa> allLsa){
		
		Id ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		long max = getValue(allLsa.elementAt(0), allLsa.elementAt(0).getFieldValue());
		ret = allLsa.elementAt(0).getId();
		
		for(int i=0; i<allLsa.size(); i++){
			long m = getValue(allLsa.elementAt(i), allLsa.elementAt(0).getFieldValue());
				if (m > max){
				max = m;
				ret =  allLsa.elementAt(i).getId();
			}
		}
		
		return ret;
		
	}
	
	/*
	public static Lsa max (Vector<Lsa> allLsa, String fieldValue){
		
		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int max = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
		ret = allLsa.elementAt(0).getCopy();
		
		for(int i=0; i<allLsa.size(); i++){
			if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() > max){
				max = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
				ret =  allLsa.elementAt(i).getCopy();
			}
		}
		
		return ret;
		
	}*/
	
	public static String maxValue (Vector<Lsa> allLsa, String fieldValue){
		
//		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int max = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
//		ret = allLsa.elementAt(0).getCopy();
		
		for(int i=0; i<allLsa.size(); i++){
			if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() > max){
				max = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
//				ret =  allLsa.elementAt(i).getCopy();
			}
		}
		
		return new Integer(max).toString();
		
	}
	
	public static Id min (Vector<Lsa> allLsa){
		
		Id ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		long min = getValue(allLsa.elementAt(0), allLsa.elementAt(0).getFieldValue());
		ret = allLsa.elementAt(0).getId();

		for(int i=0; i<allLsa.size(); i++){
			long m = getValue(allLsa.elementAt(i), allLsa.elementAt(0).getFieldValue());
			if (m < min){
			min = m;
			ret =  allLsa.elementAt(i).getId();
			}
		}
		
		return ret;
		
	}
	
/*
	public static Lsa min (Vector<Lsa> allLsa, String fieldValue){
		
		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int min = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
		ret = allLsa.elementAt(0).getCopy();
		
		for(int i=0; i<allLsa.size(); i++){
			if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() < min){
				min = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
				ret =  allLsa.elementAt(i).getCopy();
			}
		}
		
		return ret;
		
	}*/
	
	public static String minValue (Vector<Lsa> allLsa, String fieldValue){
		
//		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		int min = new Integer(allLsa.elementAt(0).getProperty(fieldValue).getValue().elementAt(0)).intValue();
//		ret = allLsa.elementAt(0).getCopy();
		
		for(int i=0; i<allLsa.size(); i++){
			if (new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue() < min){
				min = new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
//				ret =  allLsa.elementAt(i).getCopy();
			}
		}
		
		return new Integer(min).toString();
		
	}
	
	/*
	public static Lsa mean (Vector<Lsa> allLsa, String fieldValue){
		
		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int val = 0;
		
		for(int i=0; i<allLsa.size(); i++){
			System.out.println(allLsa.elementAt(i).toString());
			System.out.println(fieldValue);
				val += new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
		}
		
		
		val = val / allLsa.size();
		
		ret = allLsa.elementAt(0).getCopy();
		try {
			ret.setProperty(new Property(fieldValue, ""+val));
		} catch (UnresolvedPropertyNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String(""+new Date().getTime())));
		
		System.out.println("Lsa res "+ret.toString());
		
		return ret;
		
	}*/

	
	public static Lsa avg (Vector<Lsa> allLsa){
		
		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int val = 0;
		
		for(int i=0; i<allLsa.size(); i++){
			System.out.println(allLsa.elementAt(i).toString());
			System.out.println(allLsa.elementAt(i).getFieldValue());
				val += new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
		}
		
		
		val = val / allLsa.size();
		
		ret = allLsa.elementAt(0).getCopy();
		try {
			ret.setProperty(new Property(allLsa.elementAt(0).getFieldValue(), ""+val));
		} catch (UnresolvedPropertyNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String(""+new Date().getTime())));
		
		System.out.println("Lsa res "+ret.toString());
		
		return ret;
		
	}
	
	public static String avgValue (Vector<Lsa> allLsa, String fieldValue){
		
		Lsa ret = null;
		
		if(allLsa.isEmpty())
			return null;
		
		int val = 0;
		
		for(int i=0; i<allLsa.size(); i++){
		//	System.out.println(allLsa.elementAt(i).toString());
		//	System.out.println(fieldValue);
				val += new Integer(allLsa.elementAt(i).getProperty(fieldValue).getValue().elementAt(0)).intValue();
		}
		
		
		val = val / allLsa.size();
		
		ret = allLsa.elementAt(0).getCopy();
		try {
			ret.setProperty(new Property(fieldValue, ""+val));
		} catch (UnresolvedPropertyNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String(""+new Date().getTime())));
		
	//	System.out.println("Lsa res "+ret.toString());
		
		return new Integer(val).toString();
		
	}

}
