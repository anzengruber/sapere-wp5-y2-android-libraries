/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace.ecolaws;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.SubDescription;
import eu.sapere.middleware.node.lsaspace.EcoLawsEngine;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Bonding {
		
	public Bonding(){
		
	}
	
	public static void execBondingEcoLaws(Lsa[] allLsa, Space s, OperationManager opMng){
		
		/* Bonds can be established as follows:
		 * 	Lsa - Lsa
		 * 	Sd - Sd
		 * 	Lsa - Sd
		 * 	Sd - Lsa
		 */
		
		//System.out.println("Bonding eco-law");
		
	//	destroyBonds(allLsa, s);
		execBondsFromLSA(allLsa, s, opMng);
		execBondsFromSD(allLsa, s, opMng);
		
	}
	


	private static void execBondsFromSD(Lsa[] allLsa, Space s, OperationManager opMng){
		
		for (int i=0; i<allLsa.length;i++){
			
			SubDescription candidateSd[] = null;
			candidateSd = allLsa[i].getSubDescriptions();
			if (candidateSd != null){
				
				for(int j=0; j<candidateSd.length; j++){
					
					SubDescription candidateRequester = candidateSd[j];
					boolean continueWithSd = true;
					
					if(isBondRequester(candidateRequester)){
					
					//1. Try to match SD with LSAs
						
						Lsa[] newAllLsa = s.getAllLsa();
					
					for(int c=0; c<newAllLsa.length;c++){
						if (i != c){
							if(isBondable(newAllLsa[c])){
								if (candidateRequester.isFormalOne()){
									boolean found = candidateRequester.matches(newAllLsa[c]);
									if (found){
										Lsa candidateRequesterLsaCopy = allLsa[i].getCopy();
										SubDescription sd = candidateRequesterLsaCopy.getSubDescriptionByName(candidateRequesterLsaCopy.getSubDescriptionName(candidateRequester.getId()));
										sd.addBond(newAllLsa[c].getId().toString());
										Operation updateOp = new Operation().updateBondOperation(candidateRequesterLsaCopy, candidateRequesterLsaCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										opMng.execOperation(updateOp);
										
										Lsa allLsacCopy = newAllLsa[c].getCopy();
										allLsacCopy.addBond(candidateRequester.getId().toString());
										updateOp = new Operation().updateBondOperation(allLsacCopy, allLsacCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										opMng.execOperation(updateOp);
										
										break;
									}
									
								}
								
								else{
									
									boolean found = candidateRequester.matches(newAllLsa[c]);
									if (found){
										
										Lsa candidateRequesterLsaCopy = allLsa[i].getCopy();
										SubDescription sd = candidateRequesterLsaCopy.getSubDescriptionByName(candidateRequesterLsaCopy.getSubDescriptionName(candidateRequester.getId()));
										sd.addBond(newAllLsa[c].getId().toString());
										Operation updateOp = new Operation().updateBondOperation(candidateRequesterLsaCopy, candidateRequesterLsaCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										
										opMng.execOperation(updateOp);
												
									}
								}
								
							}//end isBondable
							
							
							SubDescription sd[] =  null;
							sd = newAllLsa[c].getSubDescriptions();
							if (sd != null){
											
							
								
							for (int k=0; k<sd.length; k++){
								
								
								if(isBondable(sd[k]) && continueWithSd){
									if (candidateRequester.isFormalOne()){
										boolean found = candidateRequester.matches(sd[k]);
										if (found){
								
											continueWithSd = false;
											Lsa allLsaiLsaCopy = allLsa[i].getCopy();
											
											
											SubDescription cr = allLsaiLsaCopy.getSubDescriptionByName(allLsa[i].getSubDescriptionName(candidateRequester.getId()));
											
											
											cr.addBond(sd[k].getId().toString());
											
											Operation updateOp = new Operation().updateBondOperation(allLsaiLsaCopy, allLsaiLsaCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
											
											opMng.execOperation(updateOp);
										
										//Use Update Operation to enable notification to agents
										Lsa allLsacCopy = newAllLsa[c].getCopy();
										cr = allLsacCopy.getSubDescriptionByName(newAllLsa[c].getSubDescriptionName(sd[k].getId()));
										cr.addBond(candidateRequester.getId().toString());
										
										
										updateOp = new Operation().updateBondOperation(allLsacCopy, allLsacCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										
										opMng.execOperation(updateOp);
												
										break;}
									}
									
									else{
										
										boolean found = candidateRequester.matches(sd[k]);
										if (found){
											
											Lsa allLsaiLsaCopy  = allLsa[i].getCopy();
																					
											SubDescription cr = allLsaiLsaCopy.getSubDescriptionByName(allLsa[i].getSubDescriptionName(candidateRequester.getId()));
																					
											cr.addBond(sd[k].getId().toString());
																				
											Operation updateOp = new Operation().updateBondOperation(allLsaiLsaCopy, allLsaiLsaCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
											opMng.execOperation(updateOp);
										
									}
									
								}
								
									
							}
							}
							
						}// end SD == null
							
						}// end i!=c
						
					}
					
					
				} // this sd does not require bonds
					
				}// end sd cicle
				
				
			} // this LSA has no SDs-- nothing done
			
		}// end for allLsa
		
	}
	

	private static void execBondsFromLSA(Lsa[] allLsa, Space s, OperationManager opMng){
		
		for (int i=0; i<allLsa.length;i++){
			
			
			Lsa candidateRequester = allLsa[i];
			boolean continueWithSd = true;
			if (isBondRequester(candidateRequester)){
				Lsa[] newAllLsa = s.getAllLsa();
				
				for(int j=0; j<newAllLsa.length; j++){
					if (i != j){ 
						if (isBondable(newAllLsa[j])){
							if (candidateRequester.isFormalOne()){
								boolean found = candidateRequester.matches(newAllLsa[j]);
								if (found){
									
									Lsa candidateRequesterCopy = candidateRequester.getCopy();
									candidateRequesterCopy.addBond(newAllLsa[j].getId().toString());
									Operation updateOp = new Operation().updateBondParametrizedOperation(candidateRequesterCopy, candidateRequesterCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
				//					System.out.println("\t"+candidateRequesterCopy);
									candidateRequester = candidateRequesterCopy;
									opMng.execOperation(updateOp);
									
									Lsa allLsajCopy = newAllLsa[j].getCopy();
									allLsajCopy.addBond(candidateRequesterCopy.getId().toString());
				//					System.out.println("\t"+allLsajCopy);
									Operation updateOp2 = new Operation().updateBondParametrizedOperation(allLsajCopy, allLsajCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
									opMng.execOperation(updateOp2);
									continueWithSd = false;

									break;
								}
									
								}
							else{
								//System.out.println("Bonding *");
								boolean found = candidateRequester.matches(newAllLsa[j]);
								boolean alreadyBonded = candidateRequester.hasBondWith(newAllLsa[j].getId());
								if (found && !alreadyBonded){
									//System.out.println("Bonding * - found - "+newAllLsa[j].getId());
									Lsa candidateRequesterCopy = candidateRequester.getCopy();
									candidateRequesterCopy.addBond(newAllLsa[j].getId().toString());
									Operation updateOp = new Operation().updateBondOperation(candidateRequesterCopy, candidateRequesterCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
									opMng.execOperation(updateOp);
									candidateRequester = candidateRequesterCopy; //////
									}
							}
							
					
						}
						
						SubDescription sd[] =  null;
						sd = newAllLsa[j].getSubDescriptions();
						if (sd != null){
							for (int k=0; k<sd.length; k++){
								
								//System.out.println("considero "+ sd[k]+" bondable? "+isBondable(sd[k]));
								
								
								if(isBondable(sd[k]) && continueWithSd){
									//System.out.println(sd[k]+" isBondable(sd[k]) ");
									//System.out.println("candidateRequester.isFormalOne() "+candidateRequester.isFormalOne());
								if (candidateRequester.isFormalOne()){
									
									boolean found = candidateRequester.matches(sd[k]);
									//System.out.println("candidateRequester.matches "+sd[k].toString()+ " "+found);
									if (found){
										continueWithSd = false;
										Lsa candidateRequesterCopy = candidateRequester.getCopy();
										candidateRequesterCopy.addBond(sd[k].getId().toString());
										Operation updateOp = new Operation().updateBondOperation(candidateRequesterCopy, candidateRequesterCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										opMng.execOperation(updateOp);
										
										Lsa sdkLsaCopy = newAllLsa[j].getCopy();
										SubDescription cr = sdkLsaCopy.getSubDescriptionByName(newAllLsa[j].getSubDescriptionName(sd[k].getId()));
										cr.addBond(candidateRequester.getId().toString());
										
										
										updateOp = new Operation().updateBondOperation(sdkLsaCopy, sdkLsaCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										
										opMng.execOperation(updateOp);
										break;}
								}
								
								else{
									
									boolean found = candidateRequester.matches(sd[k]);
									if (found){
										//System.out.println("Trovato match");
										Lsa candidateRequesterCopy = candidateRequester.getCopy();
										candidateRequesterCopy.addBond(sd[k].getId().toString());
										Operation updateOp = new Operation().updateBondOperation(candidateRequesterCopy, candidateRequesterCopy.getId(), EcoLawsEngine.ECO_LAWS_AGENT);
										
										//System.out.println(updateOp);
										opMng.execOperation(updateOp);
										candidateRequester = candidateRequesterCopy;
										
								}
								
							}
							
								
						}
						}
						
					}// end SD == null
						
						
				
						
						
						
					}// end if i!= j
				}
				
				
			}// end check LSA-LSA e LSA-SD
			
			
			
		}
		
		
	}
	
	@SuppressWarnings("unused")
	private static boolean isLsaId(String s){
		if(s.indexOf("#") != s.lastIndexOf("#"))
			return false;
		return true;
	}
	
	private static boolean isBondRequester(Lsa candidate){
		
		if(candidate.isFormal())
			if (! (candidate.isFormalOne() && candidate.hasBonds()))
				return true;
		
		return false;
	}
	
	private static boolean isBondRequester(SubDescription candidate){
		if(candidate.isFormal())
			if (! (candidate.isFormalOne() && candidate.hasBonds()))
				return true;
		
		return false;
	}
	
	private static boolean isBondable(Lsa bondCandidate){
		
		if((! bondCandidate.isFormal()) && (!bondCandidate.hasBonds() ))
			return true;
		
		if(bondCandidate.isFormalOne() && ! bondCandidate.hasBonds())
			return true;
		
		if(bondCandidate.isFormalMany())
			return true;
		
		
		return false;
	}
	
	private static boolean isBondable(SubDescription bondCandidate){
		
		if((! bondCandidate.isFormal()) && (!bondCandidate.hasBonds() ))
			return true;
		
		if(bondCandidate.isFormalOne() && ! bondCandidate.hasBonds())
			return true;
		
		if(bondCandidate.isFormalMany())
			return true;
		
		
		return false;
	}

}
