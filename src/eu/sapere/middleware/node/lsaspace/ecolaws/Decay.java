/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace.ecolaws;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.lsaspace.EcoLawsEngine;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.lsa.values.PropertyName;;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Decay {
	
	public Decay(){
		
	}
	
	public static void execDecay(Lsa[] allLsa, Space s, OperationManager opMng){

		for(int i=0; i<allLsa.length; i++){
						
			if(allLsa[i].hasDecayProperty()){
				
				
				Integer decay = allLsa[i].getDecayProperty();
				
				if (decay != null){
					
					//decrement the TTL
					decay = PropertyName.decrement(decay);
					
					
					
					if (decay.intValue() == 0){
						s.remove(allLsa[i].getId(), EcoLawsEngine.ECO_LAWS_DECAY);
						}
					else{
						Lsa copy = allLsa[i];
						copy.addProperty(new Property("decay", decay.toString()));
						Operation updateOp = new Operation().updateOperation(copy, copy.getId(), EcoLawsEngine.ECO_LAWS_DECAY);
						opMng.execOperation(updateOp);
						
					}
					
				}
				
			}
			
		}
		
	}

}
