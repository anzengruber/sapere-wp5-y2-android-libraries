/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth.android;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Timer;
import java.util.TimerTask;

import android.bluetooth.BluetoothSocket;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNetworkAnalyzer;

public class BluetoothConnectionHandler {

	private static final String USER_PROFILE_REQUEST = "UPR";
	private static final String NODE_TYPE = "pc";
	private String ipAddress;
	private String btMac;
	private PhysicalNetworkAnalyzer pna;
	private String targetMac;
	private long lastPing;
	
	private TimeoutTask timeoutTask;
	private Timer t;
	
	private class TimeoutTask extends TimerTask {
		@Override
		public void run() {
//			if(System.currentTimeMillis()-lastPing > (long)30000 && targetMac != null) {
//				pna.onPhysicalNeighbourLost(targetMac);	
//				targetMac = null;
//			}
		}		
	}
	
	public BluetoothConnectionHandler(String ipAddress,
			String btMac, PhysicalNetworkAnalyzer pna) {
		lastPing = System.currentTimeMillis();
		this.ipAddress = ipAddress;
		this.btMac = btMac;
		this.pna = pna;
		targetMac = null;
		
		timeoutTask = new TimeoutTask();
		t = new Timer();
		t.schedule(timeoutTask, 0, 30000);
	}

	public void communicate(BluetoothSocket socket) {
		InputStream inputStream;
		OutputStream outputStream;
		try {
				inputStream = socket.getInputStream();
				outputStream = socket.getOutputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
				String message = reader.readLine();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
				
				if(message.startsWith(USER_PROFILE_REQUEST)) {
					writer.write(ipAddress);
					writer.write(";");
					writer.write(btMac);
					writer.write(";");
					writer.write(NodeManager.instance().getSpaceName());
					writer.write(";");
					writer.write(NODE_TYPE);
					writer.flush();
				}
				outputStream.close();
				inputStream.close();
				
			if(targetMac == null) {
				String[] str = message.split(";");
				PhysicalNeighbour neighbor = new PhysicalNeighbour(str[2], str[1],"", str[3], str[4]);
				targetMac = str[2];
				pna.onPhysicalNeighbourFound(neighbor);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		lastPing = System.currentTimeMillis();
	}
}