/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth.android;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Pair;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNetworkAnalyzer;

public class BluetoothServerSocketListener extends Thread {

	private static final String SAPERE = "SAPERE";
	private String ipAddress;
	private BluetoothServerSocket serverSocket;
	private BluetoothSocket socket;
	private BluetoothAdapter adapter;
	private PhysicalNetworkAnalyzer pna;
	private Map<String, Pair<Long, BluetoothConnectionHandler>> handlers;
	
	public BluetoothServerSocketListener(PhysicalNetworkAnalyzer pna, String ipAddress, String uuid) {
		this.adapter = BluetoothAdapter.getDefaultAdapter();
		this.pna = pna;
		this.ipAddress = ipAddress;
		handlers = new HashMap<String, Pair<Long, BluetoothConnectionHandler>>();
		
		try {
			serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(SAPERE, UUID.fromString(uuid));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		boolean socketFound = false;
		while (true) {
			try {
				socket = serverSocket.accept();
				socketFound = true;
			} catch (Exception e) {
				socketFound = false;
			}
			
			if (socketFound) {
				String targetMac = socket.getRemoteDevice().getAddress();
				StringBuffer b = new StringBuffer(targetMac);
				b.deleteCharAt(14);b.deleteCharAt(11);b.deleteCharAt(8);b.deleteCharAt(5);b.deleteCharAt(2);
				targetMac=b.toString();
				
				if(!handlers.containsKey(targetMac)) {
					BluetoothConnectionHandler newHandler = new BluetoothConnectionHandler(ipAddress, adapter.getAddress(), pna);
					handlers.put(targetMac, new Pair<Long, BluetoothConnectionHandler>(System.currentTimeMillis(), newHandler));
				} 
				else continue;
				
				BluetoothConnectionHandler tmpHandler = handlers.get(targetMac).second;
				tmpHandler.communicate(socket);
				handlers.put(targetMac, new Pair<Long, BluetoothConnectionHandler>(System.currentTimeMillis(), tmpHandler));
			}
			
//			checkTimeout();
			
			try {
				sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeServerSocket() {
		try {
			serverSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}