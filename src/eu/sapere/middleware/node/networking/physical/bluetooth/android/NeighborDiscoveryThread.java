/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNetworkListener;

/**
 * @author Thomas Schmittner
 * 
 */
public class NeighborDiscoveryThread extends Thread {

	private static final int COMMUNICATION_PORT = 55510;
	private ServerSocket serverSocket;
	private volatile boolean running = true;
	private ArrayList<PhysicalNetworkListener> listeners;

	public NeighborDiscoveryThread() {
		listeners = new ArrayList<PhysicalNetworkListener>();
		try {
			serverSocket = new ServerSocket(COMMUNICATION_PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		Socket socket;
		while (running) {
			try {
				socket = serverSocket.accept();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(socket.getInputStream()));
				String message = reader.readLine();
				// FIXME finish parsing implementation
				System.out.println("===>> " + message);

				String[] args = message.split(";");
				DeviceStatus status = DeviceStatus.valueOf(args[0]);

				switch (status) {
				case FOUND:
					PhysicalNeighbour neighbor = new PhysicalNeighbour(args[2],
							args[1], "", args[3], args[4]);

					for (PhysicalNetworkListener l : listeners) {
						l.onPhysicalNeighbourFound(neighbor);
					}
					break;
				case STILL_PRESENT:
					break;
				case LOST:
					for (PhysicalNetworkListener l : listeners) {
						l.onPhysicalNeighbourLost(args[2]);
					}
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void requestStop() {
		running = false;
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void addPhysicalNetworkListener(PhysicalNetworkListener l) {
		listeners.add(l);
	}
}