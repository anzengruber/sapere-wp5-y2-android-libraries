/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth.android;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import android.os.Environment;
import eu.sapere.Action;
import eu.sapere.MessageLogger;
import eu.sapere.middleware.node.NodeManager;

/**
 * @author Thomas Schmittner
 * 
 */
public class RegistrationThread extends Thread {

	private static final String NODE_TYPE = "pc";
	private static final int REGISTRATION_DB_PORT = 55440;
	private static final int TIMEOUT = 3000;
	private static final String filename = "/registration.txt";
	private static final String QUIT = "quit";
	private static File downloadDir;
	private static String registrationIp;
	private String btMac;
	private volatile boolean success = false;
	private Action action;
	private boolean running = true;
	private Object lock = new Object();

	static {
		downloadDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(downloadDir + filename));
			registrationIp = reader.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public RegistrationThread(String btMac, Action action, Object lock) {
		this.btMac = btMac;
		this.action = action;
		this.lock = lock;
	}

	public void requestStop() {
		running = false;
	}

	public boolean isRegistered() {
		return success;
	}

	@Override
	public void run() {
		int count = 0;
		while (running && !success && count < 10) {
			Socket socket = null;
			try {
				socket = new Socket();
				socket.connect(new InetSocketAddress(registrationIp,
						REGISTRATION_DB_PORT), TIMEOUT);
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream()));
				writer.write(getMessage());
				writer.flush();
				writer.close();
				socket.close();
				success = true;
				if (action == Action.REGISTER) {
					MessageLogger.instance().log(
							"Registration successfully completed.");
					System.out.println("Registration successfully completed.");
				} else {
					MessageLogger.instance().log(
							"Deregistration successfully completed.");
					System.out
							.println("Deregistration successfully completed.");
					synchronized (lock) {
						lock.notifyAll();
					}
				}
			} catch (Exception e) {
				try {
					if (socket != null) {
						socket.close();
					}
					Thread.sleep(2000);
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				if (action == Action.DEREGISTER) {
					count++;
					MessageLogger.instance().log(
							"Failed to deregister at " + registrationIp
									+ " (port " + REGISTRATION_DB_PORT + ")");
				} else {
					MessageLogger.instance().log(
							"Failed to register at " + registrationIp
									+ " (port " + REGISTRATION_DB_PORT + ")");
				}
			}
		}
	}

	private String getMessage() {
		StringBuilder builder = new StringBuilder();
		String nodeName = null;
		do {
			nodeName = NodeManager.instance().getSpaceName();
		} while (nodeName == null);
		if (action == Action.REGISTER) {
			builder.append(btMac);
			builder.append(";");
			NodeManager.instance();
			builder.append(nodeName);
			builder.append(";");
			builder.append(NODE_TYPE);
		} else {
			builder.append(QUIT);
			builder.append(";");
			builder.append(btMac);
			builder.append(";");
			builder.append(nodeName);
		}
		return builder.toString();
	}
}