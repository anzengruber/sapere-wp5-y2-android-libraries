/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import eu.sapere.middleware.node.networking.topology.util.TimeLimitedCacheMap;

/**
 * @author Gabriella Castelli
 * 
 */
public class NeighbourAnalyzer implements Runnable {

	private TimeLimitedCacheMap pNeighbours = null;
	private TimeLimitedCacheMap lNeighbours = null;
	private Map<String, Object> oldNeighbours;

	private static Vector<NeighbourListener> listeners = new Vector<NeighbourListener>();
	private static Vector<NeighbourListener> appListeners = new Vector<NeighbourListener>();

	public NeighbourAnalyzer(TimeLimitedCacheMap pNeighbours,
			TimeLimitedCacheMap lNeighbours, NeighbourListener listener) {

		this.pNeighbours = pNeighbours;
		this.lNeighbours = lNeighbours;
		oldNeighbours = new HashMap<String, Object>();
		listeners.add(listener);
	}

	@SuppressWarnings("unchecked")
	public void run() {
		while (true) {
			Map<String, Object> clonedPNeighbours = pNeighbours.getClonedMap();
			Map<String, Object> clonedLNeighbours = lNeighbours.getClonedMap();
			Map<String, Object> currentNeighbours = new HashMap<String, Object>();

			Iterator<String> iterator = clonedPNeighbours.keySet().iterator();

			while (iterator.hasNext()) {
				String key = iterator.next().toString();
				HashMap<String, String> value = (HashMap<String, String>) clonedPNeighbours
						.get(key);
				if (oldNeighbours.containsKey(key)) {
					oldNeighbours.remove(key);
				}
				currentNeighbours.put(key, value);
				for (NeighbourListener l : listeners) {
					l.onNeighbourFound(key, value);
				}
				for (NeighbourListener l : appListeners) {
					l.onNeighbourFound(key, value);
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			iterator = clonedLNeighbours.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next().toString();
				HashMap<String, String> value = (HashMap<String, String>) clonedLNeighbours
						.get(key);
				if (oldNeighbours.containsKey(key)) {
					oldNeighbours.remove(key);
				}
				currentNeighbours.put(key, value);

				for (NeighbourListener l : listeners) {
					l.onNeighbourFound(key, value);
				}

				for (NeighbourListener l : appListeners) {
					l.onNeighbourFound(key, value);
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			iterator = oldNeighbours.keySet().iterator();
			while (iterator.hasNext()) {
				String oldKey = iterator.next();
				for (Iterator<NeighbourListener> it = appListeners.iterator(); it
						.hasNext();)
					it.next()
							.onNeighbourExpired(
									oldKey,
									(HashMap<String, String>) oldNeighbours
											.get(oldKey));
				System.out.println(this.getClass() + " onNeighbourExpired");
			}
			oldNeighbours.clear();
			oldNeighbours.putAll(currentNeighbours);
		}
	}

	public static void addListener(NeighbourListener listener) {
		appListeners.add(listener);
	}
}