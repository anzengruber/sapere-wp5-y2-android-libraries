/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.logical;

import java.util.HashMap;

/**
 * @author Gabriella Castelli
 * 
 */
public class LogicalNeighbour {

	private String btMac = null;
	private String ipAddress = null;
	private String neighbour = null;
	private String neighbourType = null;

	public LogicalNeighbour(String btMacAddress, String ipAddress,
			String nodeName, String nodeType) {
		this.btMac = btMacAddress;
		this.ipAddress = ipAddress;
		this.neighbour = nodeName;
		this.neighbourType = nodeType;
	}

	public HashMap<String, String> getData() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("btMac", btMac);
		map.put("ipAddress", ipAddress);
		map.put("neighbour", neighbour);
		map.put("nodeType", neighbourType);

		return map;
	}

	public String getBtUrl() {
		return btMac;
	}

	public void setBtUrl(String btMacAddress) {
		this.btMac = btMacAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getNeighbour() {
		return neighbour;
	}

	public void setNeighbour(String nodeName) {
		this.neighbour = nodeName;
	}

	public String getNeighbourType() {
		return neighbourType;
	}

	public void setNeighbourType(String nodeType) {
		this.neighbourType = nodeType;
	}
}