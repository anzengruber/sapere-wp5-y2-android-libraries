/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.logical;

/**
 * @author Gabriella Castelli
 * 
 */
public class LogicalNetworkAnalyzer {

	// private LogicalNetworkListener listener = null;

	public LogicalNetworkAnalyzer(LogicalNetworkListener listener) {
		// LogicalNeighbour ln1 = new LogicalNeighbour(null, "140.78.95.176",
		// "Thomas", "pc" );
		// LogicalNeighbour ln2 = new LogicalNeighbour(null, "140.78.95.19",
		// "NexusS", "pc" );
		// LogicalNeighbour ln3 = new LogicalNeighbour(null, "140.78.95.57",
		// "PrivateDisplayB", "pc" );
		// LogicalNeighbour ln4 = new LogicalNeighbour(null, "140.78.95.78",
		// "Bernhard", "pc" );
		// listener.onLogicalNeighbourFound(ln1);
		// listener.onLogicalNeighbourFound(ln2);
		// listener.onLogicalNeighbourFound(ln3);
		// listener.onLogicalNeighbourFound(ln4);
	}
}