/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.physical;

import java.io.Serializable;
import java.util.HashMap;

public class PhysicalNeighbour implements Serializable {

	private static final long serialVersionUID = 1L;
	private String btMac = null;
	private String ipAddress = null;
	private String btRssi = null;
	private String neighbour = null;
	private String neighbourType = null;

	public PhysicalNeighbour(String btMacAddress, String ipAddress,
			String rssi, String nodeName, String nodeType) {
		this.btMac = btMacAddress;
		this.ipAddress = ipAddress;
		this.btRssi = rssi;
		this.neighbour = nodeName;
		this.neighbourType = nodeType;
	}

	public String getBtUrl() {
		return btMac;
	}

	public void setBtUrl(String btMacAddress) {
		this.btMac = btMacAddress;
	}

	public HashMap<String, String> getData() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("btMac", btMac);
		map.put("ipAddress", ipAddress);
		map.put("btRssi", btRssi);
		map.put("neighbour", neighbour);
		map.put("nodeType", neighbourType);
		return map;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getBtRssi() {
		return btRssi;
	}

	public void setBtRssi(String btRssi) {
		this.btRssi = btRssi;
	}
}