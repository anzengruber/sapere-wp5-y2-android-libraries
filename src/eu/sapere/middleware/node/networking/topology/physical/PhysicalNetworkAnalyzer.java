/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.physical;

import android.util.Log;

public class PhysicalNetworkAnalyzer implements PhysicalNetworkListener {

	private static PhysicalNetworkListener listener = null;

	// private static BluetoothServerSocketListener serverSocketListener;
	// private boolean running = true;

	public PhysicalNetworkAnalyzer(PhysicalNetworkListener listener) {
		PhysicalNetworkAnalyzer.listener = listener;
		Log.d("App_", "PhysicalNetworkAnalyzer activated");
	}

	// @Override
	// public void run() {
	// String ipAddress = Model.instance().getLocalIpAddress();
	//
	// while (running) {
	//
	// }
	// }

	@Override
	public void onPhysicalNeighbourFound(PhysicalNeighbour neighbour) {
		listener.onPhysicalNeighbourFound(neighbour);
	}

	@Override
	public void onPhysicalNeighbourLost(String btMacAddress) {
		listener.onPhysicalNeighbourLost(btMacAddress);
	}

	// public void stopPhysicalNetworkAnalyzer() {
	// running = false;
	// if (serverSocketListener != null) {
	// serverSocketListener.requestStop();
	// }
	// }
}