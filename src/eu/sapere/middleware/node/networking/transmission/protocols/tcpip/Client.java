/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;


import java.io.ObjectOutputStream;
import java.net.Socket;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.values.NeighbourLsa;


public class Client {
	
//	Lsa deliverLsa = null;
//	Lsa remoteNode = null;
	
	
	private String port = "10007"; // DA CAMBIARE!!!!
	
	public Client(){
		
	}
	
	public void deliver (Lsa deliverLsa, Lsa destinationLsa){
//		this.deliverLsa = deliverLsa;
		Socket socket = null;
		try {		
			socket = new Socket( destinationLsa.getProperty(NeighbourLsa.IP_ADDRESS.toString()).getValue().elementAt(0), new Integer(port));
			
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(deliverLsa);
			socket.close();
		} catch (Exception e) {
			System.out.println("Server not reachable: "+NeighbourLsa.IP_ADDRESS.toString());
		}
		
	}
}
