/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;

import java.io.IOException;
import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
import java.net.Socket;

import eu.sapere.middleware.lsa.Lsa;



public class ServerThread extends Thread{
	
	private Socket socket = null;
	
	private LsaReceived listener;
	 
    public ServerThread (Socket socket, LsaReceived listener) {
    	super("Server");
    	this.socket = socket;
    	this.listener = listener;
    }
    
    public void run() {
    	 
        try {
        		System.out.println("Receiving LSA initiated");
    			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
    			Lsa receivedLsa = (Lsa) ois.readObject();
    			listener.onLsaReceived(receivedLsa);    			
    			socket.close();
    			System.out.println("Receiving LSA complete");
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ClassNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
	
}


