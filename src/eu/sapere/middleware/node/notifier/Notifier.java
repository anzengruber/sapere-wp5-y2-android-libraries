/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.notifier;

import java.util.Enumeration;
import java.util.Vector;

import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.Event;
import eu.sapere.middleware.node.notifier.event.SapereEvent;
import eu.sapere.middleware.node.notifier.exception.InvalidEventTypeException;
import eu.sapere.middleware.node.notifier.filter.BondedLsaUpdateFilter;



/**
 * 
 */

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Notifier implements INotifier {
	
	static private Notifier singleton = null;
	
	  private Class<Event> eventClass = null;
	  protected Vector<Subscription> subscriptions = null;

	  
  // Prevents direct instantiation of the event service
  private Notifier() {
    eventClass = Event.class;
    subscriptions = new Vector<Subscription>();
  }

  // Provides well-known access point to singleton EventService
  static public Notifier instance() {
    if (singleton == null)
      singleton = new Notifier();
    return singleton;
  }
  
  public void publish(SapereEvent event) {
	  
	  for (Enumeration<Subscription> elems = subscriptions.elements(); elems.hasMoreElements(); ) {
		  
      Subscription subscription = elems.nextElement();   
      
      if(subscription.eventType.equals(BondedLsaUpdateEvent.class) && (event instanceof BondedLsaUpdateEvent) ){
    	  
    	if( subscription.filter.apply(event, subscription.subscriberName)){
    		 subscription.subscriber.onNotification(event);
    	}
       }
      else{
   
      if (subscription.eventType.isAssignableFrom(event.getClass())
          && (subscription.filter == null || subscription.filter.apply(event))){
    	   
    	  subscription.subscriber.onNotification(event);

      }
        
    }}
  }
  
  public void subscribe(Subscription s)
		  throws InvalidEventTypeException{
	  
	  if (! eventClass.isAssignableFrom(s.eventType))
	        throw new InvalidEventTypeException();
	  
	  if (!subscriptions.contains(s)){
	        subscriptions.addElement(s);
	        }
	  }


  public void unsubscribe(Subscription s)
		    throws InvalidEventTypeException {
			  
			  
		      if (!eventClass.isAssignableFrom(s.eventType))
		        throw new InvalidEventTypeException();
		      subscriptions.remove(s);

  }
  

public synchronized void  unsubscribe(BondedLsaUpdateFilter filter){
	  
	  Vector<Integer> v = new Vector<Integer>();
	  
	  for(int i=0; i<subscriptions.size();i++){
		  Subscription s = subscriptions.elementAt(i);
		//  System.out.println("NOTIFIER .... "+subscriptions.elementAt(i).subscriberName+" "+subscriptions.elementAt(i).eventType);
		  if(s.filter.getClass().equals(filter.getClass())){
		//	  System.out.println(s.filter.getClass());
		  BondedLsaUpdateFilter f = (BondedLsaUpdateFilter) s.filter;
		  
		  if(f.getRequestingId().equals(filter.getRequestingId()) && f.getTargetLsaId().toString().equals(filter.getTargetLsaId().toString()))
			  v.add(i);
	  }
	  

	  }
	  
	  for(int j=0; j<v.size(); j++){
		  subscriptions.removeElementAt(v.elementAt(j));
	//	  System.out.println("\t\t\t\t removed");
	  }
	  
  }
  
  
  public String toString(){
	  String ret = null;
	  for (Enumeration<Subscription> elems = subscriptions.elements(); elems.hasMoreElements(); ) {
	      Subscription subscription = elems.nextElement();
	      
	      if (ret == null)
	    	  ret = new String();
	      ret += "\t"+subscription.subscriberName+" "+subscription.eventType+"\n";
	  }
	  
	  return ret;
  }

  public String test(){
	  return ""+Math.random();
  }




}
