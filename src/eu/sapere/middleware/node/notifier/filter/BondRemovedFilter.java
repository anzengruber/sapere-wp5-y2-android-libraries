/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.notifier.filter;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.Event;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class BondRemovedFilter implements Filter{
	
	private Id targetLsaId = null;
	private String requestingId = null;
	
	public BondRemovedFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId;
		this.requestingId = requestingId;
	}

	
	public boolean apply(Event event) {
		
		boolean ret = false;
		
	    BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
	    
	    if (((Lsa)bondRemovedEvent.getLsa()).getId().toString().equals(targetLsaId.toString()) && bondRemovedEvent.getRequiringAgent().equals(requestingId))
	    	ret = true;
		
		return ret;
	}


	public boolean apply(Event event, String lsaSubscriberId) {
		// TODO Auto-generated method stub
		return false;
	}

}
