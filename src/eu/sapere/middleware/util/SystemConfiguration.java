/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TO DO: Add the management of a configuration file
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class SystemConfiguration {

	// ----------- Logging -----------------//
	/** Logging scope to use */
	private transient Logger log = Logger.getLogger("eu.sapere"); // Default
																	// application
																	// logger

	private SystemConfiguration() {

		setLoggingLevel(Level.SEVERE);

	}

	/**
	 * Stores current configuration.
	 */
	private static SystemConfiguration instance;

	/**
	 * Retrieve current system configuration.
	 * 
	 * @return The configuration.
	 */
	public final static SystemConfiguration getInstance() {
		if (instance == null)
			instance = new SystemConfiguration();
		return instance;
	}

	/**
	 * @return the log
	 */
	public final Logger getLog() {
		return log;
	}

	/**
	 * Retrieve global logging level.
	 * 
	 * @return the loggingLevel
	 */
	public Level getLoggingLevel() {
		// to do
		return Level.INFO;
	}

	/**
	 * Store global logging level.
	 * 
	 * @param level
	 *            the loggingLevel to set
	 */
	public void setLoggingLevel(Level level) {
		Logger.getLogger("sapere").setLevel(level);

	}

}
